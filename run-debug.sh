#!/bin/bash

G_MESSAGES_DEBUG=""
G_MESSAGES_DEBUG="$G_MESSAGES_DEBUG eagle-main"
G_MESSAGES_DEBUG="$G_MESSAGES_DEBUG eagle-core"
#G_MESSAGES_DEBUG="$G_MESSAGES_DEBUG eagle-core-trigger"
G_MESSAGES_DEBUG="$G_MESSAGES_DEBUG eagle-setup"
G_MESSAGES_DEBUG="$G_MESSAGES_DEBUG eagle-openpgp"
G_MESSAGES_DEBUG="$G_MESSAGES_DEBUG eagle-xmpp"
G_MESSAGES_DEBUG="$G_MESSAGES_DEBUG eagle-abook"
G_MESSAGES_DEBUG="$G_MESSAGES_DEBUG eagle-vcard"
G_MESSAGES_DEBUG="$G_MESSAGES_DEBUG eagle-callbacks-eagle"
G_MESSAGES_DEBUG="$G_MESSAGES_DEBUG eagle-callbacks-message"
G_MESSAGES_DEBUG="$G_MESSAGES_DEBUG eagle-callbacks-groupchat"

export G_MESSAGES_DEBUG
echo "Debug logging $G_MESSAGES_DEBUG"
echo "Starting eagle..."

# gdb --args ./eagle
./eagle


