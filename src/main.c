/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file main.c
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle. If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

/*!
 * @mainpage XMPP Client eagle
 *
 * @section glib glib?
 *
 * https://developer.gnome.org/glib/stable/
 *
 * @section gpgme gpgme
 *
 * https://www.gnupg.org/documentation/manuals/gpgme/index.html
 *
 * @section gtk gtk
 *
 * https://developer.gnome.org/gtk3/stable/
 *
 */

#include "config.h"
#include "eagle.h"
#include <gtk/gtk.h>
#include <stdlib.h>

#define LOG_DOMAIN "eagle-main"

int main(int argc, char *argv[]) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Starting %s", PACKAGE_STRING);

#ifdef PACKAGE_STATUS_DEVELOPMENT
  g_log(LOG_DOMAIN, G_LOG_LEVEL_WARNING, "%s is running in development mode",
        PACKAGE_TARNAME);
#endif

  gtk_init(&argc, &argv);

  GtkBuilder *builder =
      gtk_builder_new_from_file(EAGLE_UI_FILE_APPLICATION_WINDOW);
  g_assert(builder != NULL);

  eagle_ctx_t *eagle_ctx = eagle_ctx_new(builder);
  g_assert(eagle_ctx != NULL);

  gtk_builder_connect_signals(builder, eagle_ctx);

  GObject *window =
      gtk_builder_get_object(builder, EAGLE_UI_ID_APPLICATION_WINDOW);
  gtk_widget_show(GTK_WIDGET(window));

  gtk_main();

  return EXIT_SUCCESS;
}
