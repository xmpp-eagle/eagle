/* File: ui/ui_ctrl_groupchat - created: So 17. Jan 08:15:33 CET 2021
 *
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

/*!
 * @file ui_ctrl_groupchat.h
 *
 * @authors
 * DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 * Copyright (C) 2021 DebXWoody <stefan@debxwoody.de>
 *
 */

#ifndef EAGLE_UI_CTRL_GROUPCHAT_H__
#define EAGLE_UI_CTRL_GROUPCHAT_H__

#ifdef PACKAGE_STATUS_DEVELOPMENT
#endif

#include "eagle.h"
#include <gtk/gtk.h>

void ui_groupchat_incoming_presence(eagle_ctx_t *eagle_ctx, GtkBuilder *builder,
                                    eagle_presence_t *presence);

void ui_groupchat_incoming_message(eagle_ctx_t *eagle_ctx, GtkBuilder *builder,
                                   eagle_message_t *message);

#endif // EAGLE_UI_CTRL_GROUPCHAT_H__
