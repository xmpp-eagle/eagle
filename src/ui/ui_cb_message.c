/*
 * vim: expandtab:ts=2:sts=2:sw=2
 */

/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file ui_cb_message.c
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#include "ui_cb_message.h"

#define LOG_DOMAIN "eagle-callbacks-message"

gboolean on_MessageSendButton_clicked(GObject *obj, gpointer data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "on_MessageSendButton_clicked");
  EAGLE_MESSAGE_CTX(data);
  GtkTextView *tv = GTK_TEXT_VIEW(gtk_builder_get_object(
      eagle_message_ctx->builder, "TextViewMessageCompose"));
  GtkTextBuffer *b = gtk_text_view_get_buffer(tv);
  GtkTextIter iter_s;
  gtk_text_buffer_get_start_iter(b, &iter_s);

  GtkTextIter iter_e;
  gtk_text_buffer_get_end_iter(b, &iter_e);
  char *t = gtk_text_buffer_get_text(b, &iter_s, &iter_e, FALSE);

  GtkToggleToolButton *enc = GTK_TOGGLE_TOOL_BUTTON(
      gtk_builder_get_object(eagle_message_ctx->builder, "ToggleEncrypt"));
  gboolean isEnc = gtk_toggle_tool_button_get_active(enc);
  if (isEnc) {
    eagle_send_openpgp_message(eagle_message_ctx->eagle_ctx,
                               eagle_message_ctx->xmppid, g_string_new(t));
  } else {
    eagle_send_message(eagle_message_ctx->eagle_ctx, NORMAL,
                       eagle_message_ctx->xmppid, g_string_new(t));
  }

  GtkWidget *dialog = GTK_WIDGET(gtk_builder_get_object(
      eagle_message_ctx->builder, "DialogMessageCompose"));
  gtk_widget_destroy(dialog);

  return FALSE;
}

gboolean on_MessageCancelButton_clicked(GObject *obj, gpointer data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "on_MessageCancelButton_clicked");
  EAGLE_MESSAGE_CTX(data);
  GtkWidget *dialog = GTK_WIDGET(gtk_builder_get_object(
      eagle_message_ctx->builder, "DialogMessageCompose"));
  gtk_widget_destroy(dialog);
  return FALSE;
}
