/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file ui.h
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#ifndef EAGLE_UI_H__
#define EAGLE_UI_H__

#ifdef PACKAGE_STATUS_DEVELOPMENT
#endif

#include <gtk/gtk.h>

#define EAGLE_UI_ID_TREE_VIEW_ADDRESSBOOK_OPENPGP  "TreeViewAddressBookOpenPGP"
#define EAGLE_UI_ID_TREE_VIEW_ADDRESSBOOK_XMPP  "TreeViewAddressBookXMPP"
#define EAGLE_UI_ID_TREE_VIEW_ADDRESSBOOK_ABOOK  "TreeViewAddressBookAbook"
#define EAGLE_UI_ID_TREE_VIEW_ADDRESSBOOK_VCARD  "TreeViewAddressBookVCard"

#define EAGLE_UI_ID_TOOL_BUTTON_CONNECTION  "ToolButtonConnection"


typedef enum {
	LS_AB_OPENPGP_ID = 0,
	LS_AB_OPENPGP_NAME = 1,
	LS_AB_OPENPGP_EMAIL = 2,
	LS_AB_OPENPGP_FINGERPRINT = 3,
	LS_AB_OPENPGP_EXPIRATION_DATE = 4,
	LS_AB_OPENPGP_IS_OK = 5,
	LS_AB_OPENPGP_XMPP_URI = 6,
} list_store_address_book_openpgp;

typedef enum {
	LS_AB_XMPP_ID = 0,
	LS_AB_XMPP_XMPP_URI = 1,
	LS_AB_XMPP_NAME = 2,
	LS_AB_XMPP_STATUS = 3
} list_store_address_book_xmpp;

typedef enum {
	LS_AB_ABOOK_ID = 0 ,
	LS_AB_ABOOK_FIRSTNAME = 1,
	LS_AB_ABOOK_LASTNAME = 2,
	LS_AB_ABOOK_EMAIL = 3,
	LS_AB_ABOOK_PHONE = 4,
	LS_AB_ABOOK_MOBILE = 5,
	LS_AB_ABOOK_NAME = 6,
  LS_AB_ABOOK_NICK = 7, 
  LS_AB_ABOOK_URL = 8,
  LS_AB_ABOOK_NOTES = 9,
  LS_AB_ABOOK_ADRESS = 10,
  LS_AB_ABOOK_ZIP = 11,
  LS_AB_ABOOK_STATE = 12,
  LS_AB_ABOOK_CITY = 13,
  LS_AB_ABOOK_TOOLTIP = 14
} list_store_address_book_local;

typedef enum {
 LS_AB_VCARD_ID = 0 ,
       LS_AB_VCARD_FILE = 1,
       LS_AB_VCARD_FIRSTNAME = 2,
       LS_AB_VCARD_LASTNAME = 3,
       LS_AB_VCARD_EMAIL = 4,
       LS_AB_VCARD_PHONE = 5,
       LS_AB_VCARD_MOBILE = 6,
       LS_AB_VCARD_NAME = 7,
  LS_AB_VCARD_NICK = 8, 
  LS_AB_VCARD_URL = 9,
  LS_AB_VCARD_NOTES = 10,
  LS_AB_VCARD_ADRESS = 11,
  LS_AB_VCARD_ZIP = 12,
  LS_AB_VCARD_STATE = 13,
  LS_AB_VCARD_CITY = 14,
  LS_AB_VCARD_TOOLTIP = 15
} list_store_vcard_t;


typedef enum {
	LS_MSG_INBOX_ID = 0,
	LS_MSG_INBOX_FROM = 1,
	LS_MSG_INBOX_TIMESTAMP = 2,
	LS_MSG_INBOX_PREVIEW = 3,
	LS_MSG_INBOX_IS_ENCRYPTED = 4,
	LS_MSG_INBOX_SIGNED_STATE = 5
} list_store_message_inbox_t;

typedef enum {
	LS_BLOG_MSG_ID = 0,
	LS_BLOG_MSG_SOURCE = 1,
	LS_BLOG_MSG_TIMESTAMP = 2,
	LS_BLOG_MSG_HEADLINE = 3,
	LS_BLOG_MSG_FROM = 4,
} list_store_blog_message_t;

typedef enum {
	LS_MUC_ID = 0,
	LS_MUC_JID = 1,
	LS_MUC_NAME = 2,
	LS_MUC_DESCRIPTION = 3,
} list_store_muc_t;


#define EAGLE_MESSAGE_CTX(X) \
  eagle_message_ctx_t *eagle_message_ctx = (eagle_message_ctx_t *)X; \
  g_assert(eagle_message_ctx != NULL)

#endif // EAGLE_UI_H__ 
