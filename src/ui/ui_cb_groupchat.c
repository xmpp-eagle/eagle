/* ui/ui_cb_groupchat.c - created Sa 16. Jan 09:48:54 CET 2021
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

/*!
 * @file ui_cb_groupchat.c
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 */

#include "ui_cb_groupchat.h"
#include "ui_groupchat.h"
#include <gtk/gtk.h>

#define LOG_DOMAIN "eagle-callbacks-groupchat"

void on_WindowChatWindow_show(GtkWidget *widget, gpointer user_data) {
  eagle_ui_groupchat_t *data = (eagle_ui_groupchat_t *)user_data;
  GtkBuilder *builder = data->builder;
  GtkLabel *l =
      GTK_LABEL(gtk_builder_get_object(builder, "LabelGroupchatName"));
  GString *chatname = eagle_xmpp_adr_get_full(data->groupchat->address);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Open chatgroup %s ", chatname->str);
  gtk_label_set_text(l, eagle_xmpp_adr_get_full(data->groupchat->address)->str);

  GtkTextView *tv =
      GTK_TEXT_VIEW(gtk_builder_get_object(builder, "TextViewMessages"));
  GtkTextBuffer *b = gtk_text_view_get_buffer(tv);
  GtkTextIter iter_e;
  gtk_text_buffer_get_end_iter(b, &iter_e);
  GString *x =
      g_string_new("<span background=\"#FFFFFF\" foreground=\"#AA0000\">");
  x = g_string_append(x, "========== Chat ==========\n");
  x = g_string_append(x, "</span>");
  gtk_text_buffer_insert_markup(b, &iter_e, x->str, x->len);

  GList *list = data->groupchat->message_list;
  if (!list) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "No message");
  }
  while (list) {
    eagle_message_t *m = list->data;
    GString *t = g_string_new("<b>");
    g_string_append(t, m->from->resource->str);
    g_string_append(t, " </b>");
    gtk_text_buffer_insert_markup(b, &iter_e, t->str, t->len);
    gtk_text_buffer_insert(b, &iter_e, m->message_text->str,
                           m->message_text->len);
    gtk_text_buffer_insert(b, &iter_e, "\n", 1);
    list = g_list_next(list);
  }

  g_string_free(x, TRUE);
}

gboolean on_ButtonSendMessage_clicked(GObject *obj, gpointer data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Send message");
  eagle_ui_groupchat_t *ui_groupchat = (eagle_ui_groupchat_t *)data;
  GtkBuilder *builder = ui_groupchat->builder;
  GtkTextView *tv =
      GTK_TEXT_VIEW(gtk_builder_get_object(builder, "TextViewInputMessage"));
  GtkTextBuffer *b = gtk_text_view_get_buffer(tv);
  GtkTextIter iter_s;
  gtk_text_buffer_get_start_iter(b, &iter_s);
  GtkTextIter iter_e;
  gtk_text_buffer_get_end_iter(b, &iter_e);
  GString *text =
      g_string_new(gtk_text_buffer_get_text(b, &iter_s, &iter_e, FALSE));
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Send message: %s", text->str);
  eagle_send_message(ui_groupchat->eagle_ctx, GROUPCHAT,
                     eagle_xmpp_adr_get_bare(ui_groupchat->groupchat->address),
                     text);
  return FALSE;
}

gboolean on_ButtonCancelMessage_clicked(GObject *obj, gpointer data) {
  return FALSE;
}

void on_WindowChatWindow_destroy(GtkWidget *widget, gpointer user_data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "WindowChat destroy");
  eagle_ui_groupchat_t *ui_groupchat = (eagle_ui_groupchat_t *)user_data;
  eagle_closed_groupchat(ui_groupchat->eagle_ctx,
                         ui_groupchat->groupchat->address);
}
