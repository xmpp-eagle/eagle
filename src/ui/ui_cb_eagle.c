/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file ui_cb_eagle.c
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#include "ui_cb_eagle.h"

#define LOG_DOMAIN "eagle-callbacks-eagle"

void on_ApplicationWindowEagle_show(GtkWidget *widget, gpointer user_data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "on_ApplicationWindowEagle_show");
  EAGLE_CTX(user_data);
  eagle_init(eagle_ctx);
}

// ----------------------------------------------------------------------------
// Menu Items
// ----------------------------------------------------------------------------

gboolean on_MenuItemConnect_activate(GObject *obj, gpointer data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "on_MenuItemConnect_activate");
  return FALSE;
}

gboolean on_MenuItemDisconnect_activate(GObject *obj, gpointer data) {
  return FALSE;
}

gboolean on_MenuItemQuit_activate(GObject *obj, gpointer data) {
  gtk_main_quit();
  return FALSE;
}

gboolean on_MenuItemAbout_activate(GObject *obj, gpointer data) {
  return FALSE;
}

gboolean on_AboutDialog_close(GObject *obj, gpointer data) { return FALSE; }

// ----------------------------------------------------------------------------
// Menu Items
// ----------------------------------------------------------------------------

gboolean on_TreeViewAddressBookOpenPGP_button_press_event(GtkWidget *widget,
                                                          GdkEvent *event,
                                                          gpointer data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG,
        "on_TreeViewAddressBookOpenPGP_button_press_event");
  EAGLE_CTX(data);

  if (event->button.type == GDK_2BUTTON_PRESS) {
    GtkTreeView *v = GTK_TREE_VIEW(widget);
    GtkTreeSelection *s = gtk_tree_view_get_selection(v);
    GtkTreeIter iter;
    GtkTreeModel *m = NULL;
    gchar *openpgpkey;
    GList *l = gtk_tree_selection_get_selected_rows(s, &m);
    if (gtk_tree_model_get_iter(m, &iter, l->data)) {
      gtk_tree_model_get(m, &iter, LS_AB_OPENPGP_ID, &openpgpkey, -1);
      g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Selected OpenPGP Key %s",
            openpgpkey);
      eagle_show_openpgp_details(eagle_ctx, openpgpkey);
    }
  }
  return FALSE;
}

void on_DialogOpenPGP_show(GtkWidget *widget, gpointer user_data) {
  dialog_openpgp_key_t *data = (dialog_openpgp_key_t *)user_data;
  GtkBuilder *builder = data->builder;

  gpgme_key_t key = data->key;
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "key %s", key->fpr);

  GtkLabel *fingerprint =
      (GtkLabel *)gtk_builder_get_object(builder, "Fingerprint");
  gtk_label_set_text(fingerprint, key->fpr);

  GtkTreeView *v =
      GTK_TREE_VIEW(gtk_builder_get_object(builder, "TreeViewUserID"));
  GtkListStore *s = GTK_LIST_STORE(gtk_tree_view_get_model(v));
  GtkTreeIter i;

  gpgme_user_id_t uids = key->uids;

  while (uids) {

    gtk_list_store_append(s, &i);
    gtk_list_store_set(s, &i, 0, uids->name ? uids->name : "", //
                       1, uids->email ? uids->email : "",      //
                       -1);

    if (uids->validity == GPGME_VALIDITY_FULL ||
        uids->validity == GPGME_VALIDITY_ULTIMATE) {

      gpgme_key_sig_t signatures = uids->signatures;
      while (signatures) {
        g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "signatures");
        gpgme_sig_notation_t notations = signatures->notations;
        while (notations) {
          g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "notations");
          gtk_list_store_append(s, &i);
          gtk_list_store_set(s, &i, 0,
                             notations->name ? notations->name : "",      //
                             1, notations->value ? notations->value : "", //
                             -1);
          notations = notations->next;
        }
        signatures = signatures->next;
      }
    }
    uids = uids->next;
  }

  v = GTK_TREE_VIEW(gtk_builder_get_object(builder, "TreeViewSubKeys"));
  s = GTK_LIST_STORE(gtk_tree_view_get_model(v));

  gpgme_subkey_t subkey = key->subkeys;
  while (subkey) {
    GString *usage = g_string_new("");
    if (subkey->can_certify)
      g_string_append(usage, "C");
    if (subkey->can_sign)
      g_string_append(usage, "S");
    if (subkey->can_encrypt)
      g_string_append(usage, "E");
    if (subkey->can_authenticate)
      g_string_append(usage, "A");

    time_t expires = subkey->expires;
    time_t creation = subkey->timestamp;
    GDateTime *dt_expires = g_date_time_new_from_unix_utc(expires);
    GDateTime *dt_creation = g_date_time_new_from_unix_utc(creation);

    gtk_list_store_append(s, &i);
    gtk_list_store_set(s, &i,                                       //
                       0, gpgme_pubkey_algo_string(subkey),         //
                       1, usage->str,                               //
                       2, g_date_time_format(dt_creation, "%FT%T"), //
                       3, g_date_time_format(dt_expires, "%FT%T"),  //
                       4, subkey->keyid,                            //
                       -1);
    subkey = subkey->next;
  }
}

void on_OpenPpgDialogClose_clicked(GtkWidget *widget, gpointer user_data) {
  GtkWidget *toplevel = gtk_widget_get_toplevel(GTK_WIDGET(widget));
  gtk_widget_destroy(toplevel);
}

gboolean on_ToolButtonConnection_clicked(GObject *obj, gpointer data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "on_ToolButtonConnection_clicked");
  return FALSE;
}

gboolean on_ToolButtonTLS_clicked(GObject *obj, gpointer data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "on_ToolButtonTLS_clicked");
  return FALSE;
}

gboolean on_ToolButtonOwnOpenPGP_clicked(GObject *obj, gpointer data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "on_ToolButtonOwnOpenPGP_clicked");
  return FALSE;
}

gboolean on_TreeViewMessagesInbox_button_release_event(GtkWidget *widget,
                                                       GdkEvent *event,
                                                       gpointer user_data) {
  return FALSE;
}
gboolean on_TreeViewAddressBookXMPP_button_press_event(GtkWidget *widget,
                                                       GdkEvent *event,
                                                       gpointer user_data) {
  EAGLE_CTX(user_data);
  if (event->button.type == GDK_2BUTTON_PRESS) {
    GtkTreeView *v = GTK_TREE_VIEW(widget);
    GtkTreeSelection *s = gtk_tree_view_get_selection(v);
    GtkTreeIter iter;
    GtkTreeModel *m = NULL;
    gchar *xmpp;
    GList *l = gtk_tree_selection_get_selected_rows(s, &m);
    if (gtk_tree_model_get_iter(m, &iter, l->data)) {
      gtk_tree_model_get(m, &iter, LS_AB_XMPP_ID, &xmpp, -1);
      g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Selected XMPP %s", xmpp);
      eagle_show_new_message(eagle_ctx, xmpp);
    }
  }
  return FALSE;
}

gboolean on_TextViewLog_button_release_event(GtkWidget *widget, GdkEvent *event,
                                             gpointer user_data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "on_TextViewLog_button_release_event ");
  GtkTextView *tv = GTK_TEXT_VIEW(widget);
  GtkTextBuffer *b = gtk_text_view_get_buffer(tv);
  GtkTextIter iter_e;
  gtk_text_buffer_get_end_iter(b, &iter_e);
  GString *x =
      g_string_new("<span background=\"#FFFFFF\" foreground=\"#AA0000\">");
  x = g_string_append(x, "========== MARKER ==========\n");
  x = g_string_append(x, "</span>");

  gtk_text_buffer_insert_markup(b, &iter_e, x->str, x->len);
  g_string_free(x, TRUE);
  return FALSE;
}

gboolean on_TreeViewMUCs_button_press_event(GtkWidget *widget, GdkEvent *event,
                                            gpointer data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "on_TreeViewMUCs_button_press_event");
  EAGLE_CTX(data);

  if (event->button.type == GDK_2BUTTON_PRESS) {
    GtkTreeView *v = GTK_TREE_VIEW(widget);
    GtkTreeSelection *s = gtk_tree_view_get_selection(v);
    GtkTreeIter iter;
    GtkTreeModel *m = NULL;
    gchar *groupchat;
    GList *l = gtk_tree_selection_get_selected_rows(s, &m);
    if (gtk_tree_model_get_iter(m, &iter, l->data)) {
      gtk_tree_model_get(m, &iter, LS_MUC_JID, &groupchat, -1);
      g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Selected groupchat key %s",
            groupchat);
      eagle_show_groupchat(eagle_ctx,
                           eagle_xmpp_adr_new_parse(g_string_new(groupchat)));
    }
  }
  return FALSE;
}
