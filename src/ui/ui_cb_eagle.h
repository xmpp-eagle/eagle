/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file ui_cb_eagle.h
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#ifndef EAGLE_UI_CALLBACKS_EAGLE_H__
#define EAGLE_UI_CALLBACKS_EAGLE_H__

#ifdef PACKAGE_STATUS_DEVELOPMENT
#endif

#include "eagle.h"
#include "ui.h"
#include <gtk/gtk.h>

void on_ApplicationWindowEagle_show(GtkWidget *widget, gpointer user_data);

// ---------- Menu Items ----------
gboolean on_MenuItemConnect_activate(GObject *obj, gpointer data);
gboolean on_MenuItemRefresh_activate(GObject *obj, gpointer data);
gboolean on_MenuItemDisconnect_activate(GObject *obj, gpointer data);
gboolean on_MenuItemQuit_activate(GObject *obj, gpointer data);
gboolean on_MenuItemAbout_activate(GObject *obj, gpointer data);
gboolean on_AboutDialog_close(GObject *obj, gpointer data);

// ---------- Tool Buttons ----------
gboolean on_ToolButtonConnection_clicked(GObject *obj, gpointer data);
gboolean on_ToolButtonTLS_clicked(GObject *obj, gpointer data);
gboolean on_ToolButtonOwnOpenPGP_clicked(GObject *obj, gpointer data);

gboolean on_TreeViewMessagesInbox_button_release_event(GtkWidget *widget,
                                                       GdkEvent *event,
                                                       gpointer user_data);

gboolean on_TreeViewAddressBookXMPP_button_press_event(GtkWidget *widget,
                                                       GdkEvent *event,
                                                       gpointer user_data);

void on_DialogOpenPGP_show(GtkWidget *widget, gpointer user_data);
void on_OpenPpgDialogClose_clicked(GtkWidget *widget, gpointer user_data);

gboolean on_TextViewLog_button_release_event(GtkWidget *widget, GdkEvent *event,
                                             gpointer user_data);

gboolean on_TreeViewMUCs_button_press_event(GtkWidget *widget, GdkEvent *event,
                                            gpointer user_data);

#endif // EAGLE_UI_CALLBACKS_EAGLE_H__
