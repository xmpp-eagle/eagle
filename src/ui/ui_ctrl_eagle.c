/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file ui_ctrl_eagle.c
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#include "ui_ctrl_eagle.h"
#include "ui.h"

#define LOG_DOMAIN "eagle-controller"

void set_addressbook_openpgp(eagle_ctx_t *eagle_ctx, GtkBuilder *builder,
                             GList *data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "set_addressbook_openpgp");
  GtkTreeView *v = (GtkTreeView *)gtk_builder_get_object(
      builder, EAGLE_UI_ID_TREE_VIEW_ADDRESSBOOK_OPENPGP);
  GtkListStore *s = GTK_LIST_STORE(gtk_tree_view_get_model(v));
  GtkTreeIter i;
  GList *l = data;
  while (l) {
    eagle_contact_entity_t *d = l->data;
    if (d) {
      gtk_list_store_append(s, &i);
      gtk_list_store_set(
          s, &i,                                                   //
          LS_AB_OPENPGP_ID, d->id ? d->id->str : "",               //
          LS_AB_OPENPGP_NAME, d->name->str ? d->name->str : "",    //
          LS_AB_OPENPGP_EMAIL, d->email->str ? d->email->str : "", //
          LS_AB_OPENPGP_FINGERPRINT,
          d->fingerprint->str ? d->fingerprint->str : "", //
          LS_AB_OPENPGP_EXPIRATION_DATE, d->expire ? d->expire->str : "",
          LS_AB_OPENPGP_XMPP_URI, d->xmpp ? d->xmpp->str : "", //
          LS_AB_OPENPGP_IS_OK, d->is_ok,                       //
          -1);
      l = g_list_next(l);
    }
  }
}

void set_addressbook_xmpp(eagle_ctx_t *eagle_ctx, GtkBuilder *builder,
                          GList *data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "set_addressbook_xmpp");
  GtkTreeView *v = (GtkTreeView *)gtk_builder_get_object(
      builder, EAGLE_UI_ID_TREE_VIEW_ADDRESSBOOK_XMPP);
  GtkListStore *s = GTK_LIST_STORE(gtk_tree_view_get_model(v));
  GtkTreeIter i;
  GList *l = data;
  while (l) {
    eagle_contact_entity_t *d = l->data;
    if (d) {
      gtk_list_store_append(s, &i);
      gtk_list_store_set(s, &i,                                      //
                         LS_AB_XMPP_ID, d->xmpp ? d->xmpp->str : "", //
                         LS_AB_XMPP_XMPP_URI,
                         d->xmpp->str ? d->xmpp->str : "",                  //
                         LS_AB_XMPP_NAME, d->name->str ? d->name->str : "", //
                         LS_AB_XMPP_STATUS,
                         d->notes ? d->notes->str : "", //
                         -1);
      l = g_list_next(l);
    }
  }
}

void set_addressbook_abook(eagle_ctx_t *eagle_ctx, GtkBuilder *builder,
                           GList *data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "set_addressbook_abook");
  GtkTreeView *v = (GtkTreeView *)gtk_builder_get_object(
      builder, EAGLE_UI_ID_TREE_VIEW_ADDRESSBOOK_ABOOK);
  GtkListStore *s = GTK_LIST_STORE(gtk_tree_view_get_model(v));
  GtkTreeIter i;
  GList *l = data;
  while (l) {
    eagle_contact_entity_t *d = l->data;
    if (d) {
      GString *tooltip = g_string_new("");
      if (d->name && d->name->str)
        g_string_append(tooltip, d->name->str);
      if (d->name && d->name->str)
        g_string_append(tooltip, "\n");
      if (d->lastname && d->lastname->str)
        g_string_append(tooltip, d->lastname->str);
      if (d->name && d->name->str)
        g_string_append(tooltip, "\n");
      if (d->firstname && d->firstname->str)
        g_string_append(tooltip, d->firstname->str);
      if (d->notes && d->notes->str)
        g_string_append(tooltip, d->notes->str);

      gtk_list_store_append(s, &i);
      gtk_list_store_set(
          s, &i,                                                             //
          LS_AB_ABOOK_ID, d->id ? d->id->str : "",                           //
          LS_AB_ABOOK_FIRSTNAME, d->firstname->str ? d->firstname->str : "", //
          LS_AB_ABOOK_LASTNAME, d->lastname->str ? d->lastname->str : "",    //
          LS_AB_ABOOK_EMAIL, d->email->str ? d->email->str : "",             //
          LS_AB_ABOOK_PHONE, d->phone->str ? d->phone->str : "",
          LS_AB_ABOOK_MOBILE, d->mobile->str ? d->mobile->str : "",
          LS_AB_ABOOK_NAME, d->name->str ? d->name->str : "",       //
          LS_AB_ABOOK_URL, d->url->str ? d->url->str : "",          //
          LS_AB_ABOOK_NOTES, d->notes->str ? d->notes->str : "",    //
          LS_AB_ABOOK_ADRESS, d->adress->str ? d->adress->str : "", //
          LS_AB_ABOOK_ZIP, d->zip->str ? d->zip->str : "",          //
          LS_AB_ABOOK_CITY, d->city->str ? d->city->str : "",       //
          LS_AB_ABOOK_TOOLTIP, tooltip->str ? tooltip->str : "",    //
          -1);
      l = g_list_next(l);
    }
  }
}

void set_addressbook_vcard(eagle_ctx_t *eagle_ctx, GtkBuilder *builder,
                           GList *data) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "set_addressbook_vcard");
  GtkTreeView *v = (GtkTreeView *)gtk_builder_get_object(
      builder, EAGLE_UI_ID_TREE_VIEW_ADDRESSBOOK_VCARD);
  GtkListStore *s = GTK_LIST_STORE(gtk_tree_view_get_model(v));
  GtkTreeIter i;
  GList *l = data;
  while (l) {
    eagle_contact_entity_t *d = l->data;
    if (d) {
      // GString * tooltip = g_string_new("");
      /*
          if( d->name &&  d->name->str ) g_string_append(tooltip, d->name->str);
          if( d->name && d->name->str ) g_string_append(tooltip, "\n");
          if( d->lastname &&  d->lastname->str ) g_string_append(tooltip,
         d->lastname->str); if( d->name &&d->name->str )
         g_string_append(tooltip, "\n"); if( d->firstname && d->firstname->str )
         g_string_append(tooltip, d->firstname->str);
    */
      gtk_list_store_append(s, &i);
      gtk_list_store_set(
          s, &i,                                                             //
          LS_AB_VCARD_ID, d->id ? d->id->str : "",                           //
          LS_AB_VCARD_FIRSTNAME, d->firstname->str ? d->firstname->str : "", //
          LS_AB_VCARD_LASTNAME, d->lastname->str ? d->lastname->str : "",    //
          LS_AB_VCARD_EMAIL, d->email ? d->email->str : "",                  //
          LS_AB_VCARD_PHONE, d->phone ? d->phone->str : "", LS_AB_VCARD_MOBILE,
          d->mobile ? d->mobile->str : "", LS_AB_VCARD_NAME,
          d->name ? d->name->str : "",                      //
          LS_AB_VCARD_URL, d->url ? d->url->str : "",       //
          LS_AB_VCARD_NOTES, d->notes ? d->notes->str : "", //
                                                            /*
                                                                      LS_AB_ABOOK_ADRESS, d->adress ? d->adress->str : "", //
                                                                      LS_AB_ABOOK_ZIP, d->zip ? d->zip->str : "", //
                                                                      LS_AB_ABOOK_CITY, d->city ? d->city->str : "", //
                                                                      LS_AB_ABOOK_TOOLTIP, tooltip ? tooltip->str : "", //
                                                            */
          -1);
      l = g_list_next(l);
    }
  }
}

void xmpp_connecting(eagle_ctx_t *eagle_ctx, GtkBuilder *builder) {
  GtkToolButton *b =
      GTK_TOOL_BUTTON(gtk_builder_get_object(builder, "ToolButtonConnection"));
  gtk_tool_button_set_label(b, "Connecting...");
}

void xmpp_connected(eagle_ctx_t *eagle_ctx, GtkBuilder *builder) {
  GtkToolButton *b =
      GTK_TOOL_BUTTON(gtk_builder_get_object(builder, "ToolButtonConnection"));
  gtk_tool_button_set_label(b, "Connected");
}

void message_add(eagle_ctx_t *eagle_ctx, GtkBuilder *builder,
                 eagle_message_t *message) {
  GtkTreeView *v =
      (GtkTreeView *)gtk_builder_get_object(builder, "TreeViewMessagesInbox");
  GtkListStore *s = GTK_LIST_STORE(gtk_tree_view_get_model(v));
  GtkTreeIter i;
  gtk_list_store_append(s, &i);
  gtk_list_store_set(
      s, &i,                                                          //
      LS_MSG_INBOX_FROM, eagle_xmpp_adr_get_full(message->from)->str, //
      LS_MSG_INBOX_TIMESTAMP, "",                                     //
      LS_MSG_INBOX_PREVIEW,
      message->message_text->str ? message->message_text->str : "", //
      -1);
}

void blog_message_add(eagle_ctx_t *eagle_ctx, GtkBuilder *builder,
                      eagle_blog_message_t *message) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "blog_message_add");
  GtkTreeView *v =
      (GtkTreeView *)gtk_builder_get_object(builder, "TreeViewBlogMessages");
  GtkListStore *s = GTK_LIST_STORE(gtk_tree_view_get_model(v));
  GtkTreeIter i;
  gtk_list_store_append(s, &i);
  gtk_list_store_set(
      s, &i,                                                               //
      LS_BLOG_MSG_FROM, eagle_xmpp_adr_get_full(message->from),            //
      LS_BLOG_MSG_TIMESTAMP, message->date->str ? message->date->str : "", //
      LS_BLOG_MSG_HEADLINE,
      message->title->str ? message->title->str : "", //
      -1);
}

void muc_add(eagle_ctx_t *eagle_ctx, GtkBuilder *builder,
             const eagle_groupchat_t *const muc) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Adding MUC");
  GtkTreeView *v =
      (GtkTreeView *)gtk_builder_get_object(builder, "TreeViewMUCs");
  GtkListStore *s = GTK_LIST_STORE(gtk_tree_view_get_model(v));
  GtkTreeIter i;
  gtk_list_store_append(s, &i);
  gtk_list_store_set(s, &i,                                                  //
                     LS_MUC_JID, eagle_xmpp_adr_get_full(muc->address)->str, //
                     LS_MUC_NAME, muc->name ? muc->name->str : "",           //
                     -1);
}

void log_add(eagle_ctx_t *eagle_ctx, GtkBuilder *builder, GString *message) {
  GtkTextView *tv =
      (GtkTextView *)gtk_builder_get_object(builder, "TextViewLog");
  GtkTextBuffer *b = gtk_text_view_get_buffer(tv);
  GtkTextIter iter_e;
  gtk_text_buffer_get_end_iter(b, &iter_e);

  GString *x =
      g_string_new("<span background=\"#FFFFFF\" foreground=\"#AA0000\">");
  x = g_string_append(x, message->str);
  x = g_string_append(x, "</span>");

  gtk_text_buffer_insert_markup(b, &iter_e, x->str, x->len);
  g_string_free(x, TRUE);
  GtkTextMark *marker = gtk_text_buffer_get_insert(b);
  gtk_text_view_scroll_to_mark(tv, marker, 0, FALSE, 0, 0);
}

void ui_log_presence(eagle_ctx_t *eagle_ctx, GtkBuilder *builder,
                     eagle_presence_t *presence) {
  GtkTextView *tv =
      (GtkTextView *)gtk_builder_get_object(builder, "TextViewLog");
  GtkTextBuffer *b = gtk_text_view_get_buffer(tv);
  GtkTextIter iter_e;
  gtk_text_buffer_get_end_iter(b, &iter_e);

  GString *x =
      g_string_new("<span background=\"#FFFFFF\" foreground=\"#0000FF\">");
  x = g_string_append(x, eagle_xmpp_adr_get_full(presence->from)->str);

  if (presence->type == EAGLE_PRESENCE_UNAVAILABLE) {
    x = g_string_append(x, " offline ");
  } else if (presence->type == EAGLE_PRESENCE_SUBSCRIBE) {
    x = g_string_append(x, " subscribe request");
  } else if (presence->type == EAGLE_PRESENCE_AVAILABLE) {
    switch (presence->show) {
    case EAGLE_PRESENCE_UNKOWN:
      x = g_string_append(x, " unkown ");
      break;
    case EAGLE_PRESENCE_ONLINE:
      x = g_string_append(x, " online ");
      break;
    case EAGLE_PRESENCE_AWAY:
      x = g_string_append(x, " away ");
      break;
    case EAGLE_PRESENCE_CHAT:
      x = g_string_append(x, " chat ");
      break;
    case EAGLE_PRESENCE_DND:
      x = g_string_append(x, " dnd ");
      break;
    case EAGLE_PRESENCE_XA:
      x = g_string_append(x, " xa ");
      break;
    }
  }

  if (presence->text) {
    x = g_string_append(x, " - ");
    x = g_string_append(x, presence->text->str);
  }

  x = g_string_append(x, "</span>\n");
  gtk_text_buffer_insert_markup(b, &iter_e, x->str, x->len);
  g_string_free(x, TRUE);

  GtkTextMark *marker = gtk_text_buffer_get_insert(b);
  gtk_text_view_scroll_to_mark(tv, marker, 0, FALSE, 0, 0);
}
