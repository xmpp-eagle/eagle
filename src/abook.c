/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file abook.c
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#include "abook.h"

#define LOG_DOMAIN "eagle-abook"

#define EAGLE_LOCAL_FILE "addressbook"

#define LOCAL_ABOOK "xmpp"

#define LOCAL_FIELD_NAME "name"
#define LOCAL_FIELD_EMAIL "email"
#define LOCAL_FIELD_CITY "city"
#define LOCAL_FIELD_STATE "state"
#define LOCAL_FIELD_ZIP "zip"
#define LOCAL_FIELD_COUNTRY "country"
#define LOCAL_FIELD_PHONE "phone"
#define LOCAL_FIELD_MOBILE "mobile"
#define LOCAL_FIELD_NICK "nick"
#define LOCAL_FIELD_ANNIVERSARY "anniversary"
#define LOCAL_FIELD_GROUPS "groups"
#define LOCAL_FIELD_FIRSTNAME "firstname"
#define LOCAL_FIELD_LASTNAME "lastname"
#define LOCAL_FIELD_ADDRESS_LINES "address_lines"
#define LOCAL_FIELD_BIRTHDAY "1982-02-26"
#define LOCAL_FIELD_URL "url"
#define LOCAL_FIELD_ADDRESS "address"

static GKeyFile *_get_config_key_file(const char *file);

eagle_error_code_t load_abook_adressbook(eagle_ctx_t *eagle_ctx,
                                         eagle_settings_t *settings,
                                         eagle_addressbook_t *addressbook) {
  GKeyFile *config_file = NULL;
  if (settings->local_homedir != NULL) {
    config_file = g_key_file_new();
    GError *error = NULL;
    GString *file = g_string_new(settings->local_homedir->str);
    g_string_append(file, "/");
    g_string_append(file, EAGLE_LOCAL_FILE);
    g_key_file_load_from_file(config_file, file->str, G_KEY_FILE_NONE, &error);
    g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Loading file %s", file->str);
  } else {
    config_file = _get_config_key_file(EAGLE_LOCAL_FILE);
  }
  gsize s;
  gchar **g = g_key_file_get_groups(config_file, &s);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "%ld groups found", s);

  for (int i = 0; i < s; i++) {
    GError *error = NULL;
    g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Group %s", g[i]);
    eagle_contact_entity_t *e = malloc(sizeof(eagle_contact_entity_t));
    e->id = g_string_new(g[i]);
    e->name = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_NAME, &error));
    error = NULL;
    e->firstname = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_FIRSTNAME, &error));
    error = NULL;
    e->lastname = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_LASTNAME, &error));
    error = NULL;
    e->birthday = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_BIRTHDAY, &error));
    error = NULL;
    e->email = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_EMAIL, &error));
    error = NULL;
    e->phone = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_PHONE, &error));
    error = NULL;
    e->mobile = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_MOBILE, &error));
    error = NULL;
    e->url = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_URL, &error));
    error = NULL;
    e->nick = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_NICK, &error));
    error = NULL;
    e->fingerprint = g_string_new(
        g_key_file_get_value(config_file, g[i], "fingerprint", &error));
    error = NULL;
    e->xmpp =
        g_string_new(g_key_file_get_value(config_file, g[i], "jabber", &error));
    error = NULL;
    e->notes =
        g_string_new(g_key_file_get_value(config_file, g[i], "notes", &error));
    error = NULL;
    e->adress = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_ADDRESS, &error));
    error = NULL;
    e->zip = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_ZIP, &error));
    error = NULL;
    e->state = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_STATE, &error));
    error = NULL;
    e->city = g_string_new(
        g_key_file_get_value(config_file, g[i], LOCAL_FIELD_CITY, &error));
    error = NULL;
    addressbook->list = g_list_append(addressbook->list, e);
    g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Loaded %s", e->id->str);
  }

  return EAGLE_ERR_NO_ERROR;
}

static GKeyFile *_get_config_key_file(const char *file) {
  GKeyFile *config_file = g_key_file_new();
  GError *error = NULL;
  // XDG_CONFIG_HOME
  GString *config_path = g_string_new(g_get_user_data_dir());
  g_string_append(config_path, "/eagle/");
  g_string_append(config_path, file);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Loading local adressbook from %s",
        config_path->str);
  gboolean loaded = g_key_file_load_from_file(config_file, config_path->str,
                                              G_KEY_FILE_NONE, &error);
  if (!loaded) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_ERROR, "Error loading key file: %s %s",
          config_path->str, error->message);
    return NULL;
  }
  return config_file;
}
