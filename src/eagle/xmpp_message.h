/* xmpp_message.h - created: So 10. Jan 15:22:09 CET 2021
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * @brief XMPP Message
 *
 * XMPP message related business object for eagle.
 *
 * <ul>
 *   <li>eagle_message_t</li>
 * </ul>
 *
 * @file eagle/xmpp_message.h
 *
 * @authors
 * DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 * Copyright (C) 2021 DebXWoody <stefan@debxwoody.de>
 */

/*!
 * @page eagle-xmpp-message XMPP Messages.
 *
 * XMPP Messages
 */

#ifndef EAGLE_XMPP_MESSAGE_H__
#define EAGLE_XMPP_MESSAGE_H__

#include "eagle/xmpp_base.h"
#include <glib.h>

/*!
 * @brief XMPP Message
 *
 */
typedef struct {
  eagle_xmpp_adr_t *from;
  eagle_xmpp_adr_t *to;
  GString *message_text;
  gboolean is_groupchat;
  gboolean is_signed;
  gboolean is_valid;
} eagle_message_t;

/*!
 * @brief Creates a new XMPP-Message
 * @return new allocated message
 */
eagle_message_t *eagle_xmpp_message_new();

#endif // EAGLE_XMPP_MESSAGE_H__
