/* xmpp_message.c - created: So 10. Jan 15:27:11 CET 2021
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file eagle/xmpp_message.c
 *
 * @authors
 * DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 */

#include "eagle/xmpp_message.h"
#include "xmpp.h"

#define LOG_DOMAIN "eagle-xmpp-message"

eagle_message_t *eagle_xmpp_message_new() {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "New Message");
  eagle_message_t *message = malloc(sizeof(eagle_message_t));
  message->from = NULL;
  message->to = NULL;
  message->message_text = NULL;
  message->is_groupchat = FALSE;
  message->is_signed = FALSE;
  message->is_valid = FALSE;
  return message;
}
