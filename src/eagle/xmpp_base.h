/* xmpp_base.h - created: Sa 9. Jan 07:25:29 CET 2021
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * @brief Basics for eagle's XMPP features.
 *
 * Basic XMPP related business object for eagle.
 *
 * <ul>
 *   <li>The struct eagle_xmpp_adr_t is used for full and bare
 * XMPP-Addresses</li>
 * </ul>
 *
 * @file eagle/xmpp_base.h
 *
 * @authors
 * DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 */

/*!
 * @page eagle-xmpp-base Basics for XMPP.
 *
 */

#ifndef EAGLE_XMPP_BASE_H__
#define EAGLE_XMPP_BASE_H__

#include <glib.h>

typedef enum { NORMAL, CHAT, GROUPCHAT } message_type_t;

/*!
 * @brief XMPP Address - RFC 6122
 *
 * Extensible Messaging and Presence Protocol (XMPP): Address Format
 * https://tools.ietf.org/html/rfc6122
 *
 * <pre>
   The term "bare JID" refers to an XMPP address of the form
   <localpart@domainpart> (for an account at a server) or of the form
   <domainpart> (for a server).

   The term "full JID" refers to an XMPP address of the form
   <localpart@domainpart/resourcepart> (for a particular authorized
   client or device associated with an account) or of the form
   <domainpart/resourcepart> (for a particular resource or script
   associated with a server).
   </pre>
 *
 */
typedef struct {
  /* local part of the XMPP address */
  GString *local;
  /* doamin part of the XMPP address */
  GString *domain;
  /* resource part of the XMPP address */
  GString *resource;
} eagle_xmpp_adr_t;

/*!
 * @brief Creates a new XMPP-Address
 * @param local local_part of the address
 * @param domain domain_part of the address
 * @param resource resource_part of the address or NULL for bare XMPP Addresses
 */
eagle_xmpp_adr_t *eagle_xmpp_adr_new(GString *local, GString *domain,
                                     GString *resource);

/*!
 * @brief Parse XMPP Address String
 *
 * @param address GString reference with XMPP address
 * @return XMPP Address struct
 */
eagle_xmpp_adr_t *eagle_xmpp_adr_new_parse(const GString *const address);

GString *eagle_xmpp_adr_get_bare(const eagle_xmpp_adr_t *const address);

GString *eagle_xmpp_adr_get_full(const eagle_xmpp_adr_t *const address);

gboolean eagle_xmpp_adr_compare(const eagle_xmpp_adr_t *const adr1,
                                const eagle_xmpp_adr_t *const adr2);

gboolean eagle_xmpp_adr_compare_bare(const eagle_xmpp_adr_t *const adr1,
                                     const eagle_xmpp_adr_t *const adr2);

#endif // EAGLE_XMPP_H__
