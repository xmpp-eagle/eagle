/* xmpp_groupchat.h - created: So 10. Jan 11:10:15 CET 2021
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * @brief XMPP Groupchat
 *
 * XMPP groupchat related business object for eagle.
 *
 * <ul>
 *   <li>eagle_groupchat_t</li>
 * </ul>
 *
 * @file eagle/xmpp_groupchat.h
 *
 * @authors
 * DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 * Copyright (C) 2021 DebXWoody <stefan@debxwoody.de>
 */

/*!
 * @page eagle-xmpp-groupchat XMPP Groupchat.
 *
 * XMM Groupchat
 */

#ifndef EAGLE_XMPP_GROUPCHAT_H__
#define EAGLE_XMPP_GROUPCHAT_H__

#include "eagle.h"
#include "eagle/xmpp_base.h"
#include <glib.h>
#include <strophe.h>

/*!
 * @brief XMPP Multi User chat (groupchat).
 *
 */
typedef struct {
  /*! XMPP address */
  eagle_xmpp_adr_t *address;
  GString *name;
  GString *nick;
  GList *presence_list;
  GList *message_list;
} eagle_groupchat_t;

/*!
 * @brief Creates a new XMPP-Address
 * @param address Groupchat's XMPP address
 * @return new allocated group chat struct
 */
eagle_groupchat_t *eagle_xmpp_groupchat_new(eagle_xmpp_adr_t *const address);

/*!
 * @brief Join the groupchat.
 * @param eagle eagle conect
 * @param groupchat Groupchat
 */
void eagle_xmpp_groupchat_join(const eagle_ctx_t *const eagle,
                               xmpp_conn_t *const xmpp_conn,
                               const eagle_groupchat_t *const groupchat);

eagle_groupchat_t *
eagle_xmpp_groupchat_receive_message(eagle_xmpp_adr_t *address);

eagle_groupchat_t *
eagle_xmpp_groupchat_receive_presence(eagle_xmpp_adr_t *address);

#endif // EAGLE_XMPP_GROUPCHAT_H__
