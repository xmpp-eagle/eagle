/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file eagle/xmpp_contact.h
 * Sa 9. Jan 07:07:50 CET 2021
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EAGLE_CONTACT_H__
#define EAGLE_CONTACT_H__

#include "config.h"
#include <glib.h>

#include "eagle/xmpp_base.h"

typedef struct {
  gboolean updated;
  GList *list;
} eagle_roster_t;

/* buddy */
typedef struct {
  eagle_xmpp_adr_t *xmpp_adr;
  GString *nickname;
  GHashTable *resources;
} eagle_buddy_t;

/*!
 *
 * https://tools.ietf.org/html/rfc6121#section-4.7.1
 */
typedef enum {
  EAGLE_PRESENCE_AVAILABLE = 0,
  EAGLE_PRESENCE_ERROR = 1,
  EAGLE_PRESENCE_SUBSCRIBE = 2,
  EAGLE_PRESENCE_SUBSCRIBED = 3,
  EAGLE_PRESENCE_UNAVAILABLE = 4,
  EAGLE_PRESENCE_UNSUBSCRIBE = 5,
  EAGLE_PRESENCE_UNSUBSCRIBED = 6
} eagle_presence_type_t;

typedef enum {
  EAGLE_PRESENCE_UNKOWN = 0,
  EAGLE_PRESENCE_ONLINE = 1,
  EAGLE_PRESENCE_AWAY = 2,
  EAGLE_PRESENCE_CHAT = 3,
  EAGLE_PRESENCE_DND = 4,
  EAGLE_PRESENCE_XA = 5,
} eagle_presence_show_t;

typedef struct {
  eagle_presence_type_t type;
  eagle_xmpp_adr_t *from;
  GString *since;
  eagle_presence_show_t show;
  GString *text;
} eagle_presence_t;

eagle_presence_t *eagle_presence_new(eagle_xmpp_adr_t *from,
                                     eagle_presence_type_t type,
                                     eagle_presence_show_t show, GString *text);

#endif // EAGLE_CONTACT_H__
