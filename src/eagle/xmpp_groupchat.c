/* xmpp_groupchat.c - created: So 10. Jan 11:23:17 CET 2021
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file eagle/xmpp_groupchat.c.c
 *
 * @authors
 * DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 */

#include "eagle/xmpp_groupchat.h"
#include "xmpp.h"

#define LOG_DOMAIN "eagle-xmpp-groupchat"

eagle_groupchat_t *eagle_xmpp_groupchat_new(eagle_xmpp_adr_t *const address) {
  g_assert(address);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "New Groupchat");
  eagle_groupchat_t *groupchat = malloc(sizeof(eagle_groupchat_t));
  groupchat->address = address;
  groupchat->name = NULL;
  groupchat->nick = NULL;
  groupchat->presence_list = NULL;
  groupchat->message_list = NULL;
  return groupchat;
}

void eagle_xmpp_groupchat_join(const eagle_ctx_t *const eagle,
                               xmpp_conn_t *const xmpp_conn,
                               const eagle_groupchat_t *const groupchat) {
  // groupchat->address = eagle_xmpp_adr_get_bare(fulladr);
  const char *jid = xmpp_conn_get_jid(xmpp_conn);
  gchar **jidarray = g_strsplit(jid, "@", 2);
  GString *x = g_string_new(eagle_xmpp_adr_get_bare(groupchat->address)->str);
  x = g_string_append(x, "/");
  x = g_string_append(x, jidarray[0]);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Join %s", x->str);
  join_muc(eagle, xmpp_conn, x->str);
}

eagle_groupchat_t *
eagle_xmpp_groupchat_receive_message(eagle_xmpp_adr_t *address) {
  return NULL;
}

eagle_groupchat_t *
eagle_xmpp_groupchat_receive_presence(eagle_xmpp_adr_t *address) {
  return NULL;
}
