/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file core.c
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle. If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#include "eagle/xmpp_core.h"

eagle_xmpp_adr_t* eagle_xmpp_adr_new_parse(const GString* const address) {
  g_assert(address);
  GString *local = NULL, *domain = NULL, *resource = NULL;
  gchar** splitlocal = g_strsplit(address->str, "@", 2);
  if( splitlocal[0] && splitlocal[1] ) {
    local = g_string_new(splitlocal[0]);
    gchar** domainsplit = g_strsplit(splitlocal[1], "/", 2);

    if( domainsplit[0] ) {
      domain = g_string_new(domainsplit[0]);
      if(domainsplit[1]) {
        resource = g_string_new(domainsplit[1]);
      }
    }
  }
  
  return eagle_xmpp_adr_new(local, domain, resource);
}

eagle_xmpp_adr_t* eagle_xmpp_adr_new(GString* local, GString* domain, GString *resource){
  g_assert(local && local->str);
  g_assert(domain && domain->str);
  eagle_xmpp_adr_t *adr = malloc(sizeof(eagle_xmpp_adr_t));
  adr->local = local;
  adr->domain = domain;  
  adr->resource = resource;
  return adr;
}

GString* eagle_xmpp_adr_get_bare(const eagle_xmpp_adr_t* const address){
  g_assert(address);
  GString * bare = g_string_new(address->local->str);
  bare = g_string_append(bare, "@");
  g_string_append(bare, address->domain->str);
  return bare;
}

GString* eagle_xmpp_adr_get_full(const eagle_xmpp_adr_t* const address){
  g_assert(address);
  GString * full = eagle_xmpp_adr_get_bare(address);
  if(address->resource) {
    full = g_string_append(full, "/");
    g_string_append(full, address->resource->str);
  }
  return full;
}

