/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file eagle.h
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#ifndef EAGLE_H__
#define EAGLE_H__

#include "config.h"
#include "glib.h"
#include <gpgme.h>
#include <gtk/gtk.h>

// Define the path where the gtk *.ui files are located in EAGLE_UI_PATH

#ifdef PACKAGE_STATUS_DEVELOPMENT
#define EAGLE_UI_PATH "./ui"
#else
#define EAGLE_UI_PATH EAGLEDATADIR
#endif

// Gtk UI files

#define EAGLE_UI_FILE_APPLICATION_WINDOW EAGLE_UI_PATH "/eagle.ui"
#define EAGLE_UI_FILE_OPENPGP EAGLE_UI_PATH "/openpgp.ui"
#define EAGLE_UI_FILE_MESSAGE EAGLE_UI_PATH "/message.ui"
#define EAGLE_UI_FILE_GROUPCHAT EAGLE_UI_PATH "/chat.ui"

#define EAGLE_UI_ID_APPLICATION_WINDOW "ApplicationWindowEagle"

typedef struct _eagle_ctx_t eagle_ctx_t;

/*!
 * \brief eagle Settings
 *
 * Settings of eagle application.
 * Those settings are stored in the configuration files of eagle.
 */
typedef struct {
  /*! Home directory of GnuPG */
  GString *gnupg_homedir;
  /*! Home directory of local address book */
  GString *local_homedir;
  /*! Home directory of VCard storage */
  GString *vcard_homedir;
} eagle_settings_t;

typedef struct {
  /*! XMPP address of the XMPP account */
  GString *jid;
  /*! Password of the XMPP account */
  GString *pwd;
  /*! TLS trust */
  gboolean tls_trust;
  /*! XMPP auto connect */
  gboolean xmpp_auto_connect;
} eagle_account_t;

/*!
 * \brief Error codes
 *
 */
typedef enum {
  EAGLE_ERR_NO_ERROR = 0,
  EAGLE_ERR_LOAD_DEFAULT_CONFIG = 1,
  EAGLE_ERR_INIT_GPGME = 2,
} eagle_error_code_t;

/*!
 * \brief Entity contact
 *
 */
typedef struct {
  GString *id;
  GString *name;
  GString *firstname;
  GString *lastname;
  GString *birthday;
  GString *email;
  GString *phone;
  GString *mobile;
  GString *url;
  GString *nick;
  GString *fingerprint;
  GString *xmpp;
  GString *notes;
  GString *adress;
  GString *zip;
  GString *state;
  GString *city;
  GString *expire;
  gboolean is_ok;
} eagle_contact_entity_t;

typedef struct {
  GList *list;
} eagle_addressbook_t;

#include "eagle/xmpp_base.h"
#include "eagle/xmpp_contact.h"
#include "eagle/xmpp_groupchat.h"
#include "eagle/xmpp_message.h"

typedef struct {
  GtkBuilder *builder;
  GString *keyid;
  eagle_ctx_t *eagle_ctx;
  gpgme_key_t key;
} dialog_openpgp_key_t;

eagle_ctx_t *eagle_ctx_new(GtkBuilder *builder);
eagle_error_code_t eagle_init(eagle_ctx_t *eagle_ctx);

typedef struct {
  /*! XMPP address */
  GString *xmppid;
  eagle_ctx_t *eagle_ctx;
  GtkBuilder *builder;
} eagle_message_ctx_t;

typedef struct {
  eagle_xmpp_adr_t *from;
  GString *date;
  GString *source;
  GString *title;
} eagle_blog_message_t;

/*!
 * Conference Bookmark
 */
typedef struct {
  /*! XMPP address */
  GString *jid;
  GString *name;
  gboolean autojoin;
  GString *nick;
} eagle_bookmark_conference_t;

typedef struct {
  /*! XMPP address */
  GString *jid;
  GString *url;
} eagle_bookmark_url_t;

void eagle_show_openpgp_details(eagle_ctx_t *eagle_ctx, char *openpgpkey);

void eagle_show_new_message(eagle_ctx_t *eagle_ctx, char *xmppid);

void eagle_send_openpgp_message(eagle_ctx_t *eagle_ctx, GString *to,
                                GString *text);

void eagle_send_message(eagle_ctx_t *eagle_ctx, message_type_t type,
                        GString *to, GString *text);

eagle_blog_message_t *eagle_blog_message_new(GString *from, GString *date,
                                             GString *title);

void eagle_add_message(eagle_ctx_t *eagle, eagle_message_t *message);

void eagle_add_blog_message(eagle_ctx_t *eagle, eagle_blog_message_t *message);

void eagle_add_conference_bookmark(
    eagle_ctx_t *eagle, eagle_bookmark_conference_t *conference_bookmark);

void eagle_add_url_bookmark(eagle_ctx_t *eagle,
                            eagle_bookmark_url_t *url_bookmark);

/**
 * \brief Join a Multi User Chat.
 *
 * \param eagle the eagle context
 * \param fulladr the fill xmpp address
 */
void eagle_groupchat_join(eagle_ctx_t *eagle, eagle_xmpp_adr_t *fulladr);

void eagle_log_presence(eagle_ctx_t *eagle, eagle_presence_t *presence);

void eagle_log(eagle_ctx_t *eagle, GString *message);
void eagle_show_groupchat(eagle_ctx_t *eagle_ctx, eagle_xmpp_adr_t *chatgroup);
void eagle_closed_groupchat(eagle_ctx_t *eagle_ctx,
                            eagle_xmpp_adr_t *chatgroup);

#define EAGLE_CTX(X)                                                           \
  eagle_ctx_t *eagle_ctx = (eagle_ctx_t *)X;                                   \
  g_assert(eagle_ctx != NULL)

#endif // EAGLE_H__
