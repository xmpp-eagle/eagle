/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file xmpp.c
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#include "xmpp.h"
#include "eagle/xmpp_message.h"
#include "openpgp.h"
#include <glib.h>

#define LOG_DOMAIN "eagle-xmpp"

static int _roster_handle_reply(xmpp_conn_t *const conn,
                                xmpp_stanza_t *const stanza,
                                void *const userdata);

static int _bookmark_handle_reply(xmpp_conn_t *const conn,
                                  xmpp_stanza_t *const stanza,
                                  void *const userdata);

int _incoming_iq(xmpp_conn_t *const conn, xmpp_stanza_t *const stanza,
                 void *const userdata);

static xmpp_stanza_t *_openpgp_signcrypt_stanza(xmpp_conn_t *conn, char *to,
                                                char *text);

void setup_iq_handler(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn) {
  xmpp_handler_add(conn, _incoming_iq, NULL, "iq", "get", eagle_ctx);
}

static int _incoming_message_handler(xmpp_conn_t *const conn,
                                     xmpp_stanza_t *const stanza,
                                     void *const userdata);

static int _incoming_presence_handler(xmpp_conn_t *const conn,
                                      xmpp_stanza_t *const stanza,
                                      void *const userdata);

// rfc6121 - 4. Exchanging Presence Information

void send_presence(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Sending presence");
  xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
  xmpp_stanza_t *presence = xmpp_presence_new(ctx);
  char *id = xmpp_uuid_gen(ctx);
  xmpp_stanza_set_id(presence, id);
  xmpp_handler_add(conn, _incoming_message_handler, NULL, "message", NULL,
                   eagle_ctx);
  xmpp_handler_add(conn, _incoming_presence_handler, NULL, "presence", NULL,
                   eagle_ctx);
  xmpp_send(conn, presence);
  xmpp_stanza_release(presence);
}

// XEP-0280: Message Carbons
// https://xmpp.org/extensions/xep-0280.html

void send_carbons(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Sending carbons");
  xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
  xmpp_stanza_t *carbons =
      xmpp_iq_new(ctx, EAGLE_XMPP_TYPE_SET, xmpp_uuid_gen(ctx));
  xmpp_stanza_t *enable = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(enable, "enable");
  xmpp_stanza_set_ns(enable, EAGLE_XMPP_NS_CARBONS_2);
  xmpp_stanza_add_child(carbons, enable);
  xmpp_send(conn, carbons);
}

// FIXME: Bookmarks is a pubsub item
void request_bookmarks(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Request Bookmarks");
  xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
  char *id = xmpp_uuid_gen(ctx);
  xmpp_stanza_t *iq = xmpp_iq_new(ctx, EAGLE_XMPP_TYPE_GET, id);
  xmpp_stanza_t *pubsub, *items;
  pubsub = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(pubsub, "pubsub");
  xmpp_stanza_set_ns(pubsub, "http://jabber.org/protocol/pubsub");
  xmpp_stanza_add_child(iq, pubsub);
  items = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(items, "items");
  xmpp_stanza_set_attribute(items, "node", "storage:bookmarks");
  xmpp_stanza_add_child(pubsub, items);
  xmpp_id_handler_add(conn, _bookmark_handle_reply, id, eagle_ctx);
  xmpp_send(conn, iq);
  xmpp_stanza_release(iq);
}

static int _bookmark_handle_reply(xmpp_conn_t *const conn,
                                  xmpp_stanza_t *const stanza,
                                  void *const userdata) {
  eagle_ctx_t *eagle_ctx = (eagle_ctx_t *)userdata;

  const char *type = NULL;
  type = xmpp_stanza_get_type(stanza);
  if (strcmp(type, EAGLE_XMPP_TYPE_ERROR) == 0) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_WARNING, "Bookmark query request failed");
  } else {
    xmpp_stanza_t *pubsub = xmpp_stanza_get_child_by_name_and_ns(
        stanza, "pubsub", "http://jabber.org/protocol/pubsub");
    if (pubsub) {

      /*
        <items node='storage:bookmarks'>
          <item id='current'>
            <storage xmlns='storage:bookmarks'>
              <conference name='The Play&apos;s the Thing'
                          autojoin='true'
                          jid='theplay@conference.shakespeare.lit'>
                <nick>JC</nick>
              </conference>

    */

      xmpp_stanza_t *items = xmpp_stanza_get_child_by_name(pubsub, "items");
      if (items) {
        xmpp_stanza_t *item = xmpp_stanza_get_child_by_name(items, "item");
        if (item) {
          xmpp_stanza_t *storage = xmpp_stanza_get_child_by_name_and_ns(
              item, "storage", "storage:bookmarks");
          if (storage) {
            g_log(LOG_DOMAIN, G_LOG_LEVEL_WARNING, "Bookmark found");

            xmpp_stanza_t *conference = xmpp_stanza_get_children(storage);
            while (conference) {
              const char *name = xmpp_stanza_get_attribute(conference, "name");
              const char *jid = xmpp_stanza_get_attribute(conference, "jid");
              const char *autojoin =
                  xmpp_stanza_get_attribute(conference, "autojoin");
              g_log(LOG_DOMAIN, G_LOG_LEVEL_WARNING, "Conference: %s %s %s",
                    name, jid, autojoin);

              eagle_bookmark_conference_t *conference_bookmark =
                  malloc(sizeof(eagle_bookmark_conference_t));
              conference_bookmark->jid = g_string_new(jid);
              conference_bookmark->name = g_string_new(name);

              if (autojoin && strcmp(autojoin, "true") == 0) {
                conference_bookmark->autojoin = TRUE;
              } else {
                conference_bookmark->autojoin = FALSE;
              }
              eagle_add_conference_bookmark(eagle_ctx, conference_bookmark);
              conference = xmpp_stanza_get_next(conference);
            }
          }
        }
      }
    }
  }
  return FALSE;
}

void request_roster(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn,
                    eagle_roster_t *roster) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Requesting XMPP roster");
  xmpp_ctx_t *ctx = xmpp_conn_get_context(conn);
  xmpp_stanza_t *iq, *query;
  char *id = xmpp_uuid_gen(ctx);
  iq = xmpp_iq_new(ctx, EAGLE_XMPP_TYPE_GET, id);
  query = xmpp_stanza_new(ctx);
  xmpp_stanza_set_name(query, EAGLE_XMPP_STANZA_NAME_QUERY);
  xmpp_stanza_set_ns(query, XMPP_NS_ROSTER);
  xmpp_stanza_add_child(iq, query);
  xmpp_stanza_release(query);
  xmpp_id_handler_add(conn, _roster_handle_reply, id, roster);
  xmpp_send(conn, iq);
  xmpp_stanza_release(iq);
}

int _roster_handle_reply(xmpp_conn_t *const conn, xmpp_stanza_t *const stanza,
                         void *const userdata) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Received XMPP roster");
  eagle_roster_t *roster = (eagle_roster_t *)userdata;
  xmpp_stanza_t *query, *item;
  const char *type = NULL;
  type = xmpp_stanza_get_type(stanza);

  if (strcmp(type, EAGLE_XMPP_TYPE_ERROR) == 0) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_WARNING, "Roster query request failed");
  } else {
    query = xmpp_stanza_get_child_by_name(stanza, EAGLE_XMPP_STANZA_NAME_QUERY);
    for (item = xmpp_stanza_get_children(query); item;
         item = xmpp_stanza_get_next(item)) {
      g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "\t %s (%s) sub=%s",
            xmpp_stanza_get_attribute(item, EAGLE_XMPP_STANZA_ATT_NAME),
            xmpp_stanza_get_attribute(item, "jid"),
            xmpp_stanza_get_attribute(item, "subscription"));
      eagle_contact_entity_t *r = malloc(sizeof(eagle_contact_entity_t));
      r->name = g_string_new(
          xmpp_stanza_get_attribute(item, EAGLE_XMPP_STANZA_ATT_NAME));
      r->xmpp = g_string_new(
          xmpp_stanza_get_attribute(item, EAGLE_XMPP_STANZA_ATT_JID));
      r->notes = g_string_new(
          xmpp_stanza_get_attribute(item, EAGLE_XMPP_STANZA_ATT_SUBSCRIPTION));
      roster->list = g_list_append(roster->list, r);
    }
  }
  roster->updated = TRUE;
  return 0;
}

void xmpp_send_openpgp_message(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn,
                               gpgme_ctx_t gpgme_ctx, GString *to,
                               GString *text) {
  xmpp_stanza_t *message;
  char *id = xmpp_uuid_gen(xmpp_conn_get_context(conn));
  message = xmpp_message_new(xmpp_conn_get_context(conn), NULL, to->str, id);
  xmpp_message_set_body(
      message, "This message is *encrypted* with OpenPGP (See :XEP:`0373`)");
  xmpp_stanza_set_type(message, "chat");
  xmpp_stanza_t *openpgp = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(openpgp, "openpgp");
  xmpp_stanza_set_ns(openpgp, "urn:xmpp:openpgp:0");

  xmpp_stanza_t *signcrypt =
      _openpgp_signcrypt_stanza(conn, to->str, text->str);

  char *c;
  size_t s;
  xmpp_stanza_to_text(signcrypt, &c, &s);

  GString *xmpp_to = g_string_new("xmpp:");
  g_string_append(xmpp_to, to->str);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Looking for key %s\n", xmpp_to->str);

  gpgme_key_t to_key;
  openpgp_lookup_key(gpgme_ctx, xmpp_to->str, &to_key);

  GString *xmpp_from = g_string_new("xmpp:");
  g_string_append(xmpp_from, xmpp_conn_get_jid(conn));
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Looking for key %s\n", xmpp_from->str);
  gpgme_key_t from_key;
  openpgp_lookup_key(gpgme_ctx, xmpp_from->str, &from_key);

  char *signcrypt_e = openpgp_signcrypt(gpgme_ctx, from_key, to_key, c);
  if (signcrypt_e == NULL) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_ERROR, "Message not signcrypted.");
    return;
  }
  // BASE64_OPENPGP_MESSAGE
  xmpp_stanza_t *base64_openpgp_message =
      xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_text(base64_openpgp_message, signcrypt_e);
  xmpp_stanza_add_child(openpgp, base64_openpgp_message);
  xmpp_stanza_add_child(message, openpgp);

  xmpp_stanza_to_text(message, &c, &s);

  xmpp_send(conn, message);
}
void xmpp_send_message(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn,
                       const char *type, GString *to, GString *text) {
  xmpp_stanza_t *message;
  char *id = xmpp_uuid_gen(xmpp_conn_get_context(conn));
  message = xmpp_message_new(xmpp_conn_get_context(conn), NULL, to->str, id);
  xmpp_message_set_body(message, text->str);
  xmpp_stanza_set_type(message, type);
  xmpp_send(conn, message);
}

xmpp_stanza_t *_openpgp_signcrypt_stanza(xmpp_conn_t *conn, char *to,
                                         char *text) {

  time_t now = time(NULL);
  struct tm *tm = localtime(&now);
  char buf[255];
  strftime(buf, sizeof(buf), "%FT%T%z", tm);
  int randnr = rand() % 5;
  char rpad_data[randnr];
  for (int i = 0; i < randnr - 1; i++) {
    rpad_data[i] = 'c';
  }
  rpad_data[randnr - 1] = '\0';

  // signcrypt
  xmpp_stanza_t *signcrypt = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(signcrypt, "signcrypt");
  xmpp_stanza_set_ns(signcrypt, "urn:xmpp:openpgp:0");
  // to
  xmpp_stanza_t *s_to = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(s_to, "to");
  xmpp_stanza_set_attribute(s_to, "jid", to);
  // time
  xmpp_stanza_t *time = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(time, "time");
  xmpp_stanza_set_attribute(time, "stamp", buf);
  xmpp_stanza_set_name(time, "time");
  // rpad
  xmpp_stanza_t *rpad = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(rpad, "rpad");
  xmpp_stanza_t *rpad_text = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_text(rpad_text, rpad_data);
  // payload
  xmpp_stanza_t *payload = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(payload, "payload");
  // body
  xmpp_stanza_t *body = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(body, "body");
  xmpp_stanza_set_ns(body, "jabber:client");
  // text
  xmpp_stanza_t *body_text = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_text(body_text, text);
  xmpp_stanza_add_child(signcrypt, s_to);
  xmpp_stanza_add_child(signcrypt, time);
  xmpp_stanza_add_child(signcrypt, rpad);
  xmpp_stanza_add_child(rpad, rpad_text);
  xmpp_stanza_add_child(signcrypt, payload);
  xmpp_stanza_add_child(payload, body);
  xmpp_stanza_add_child(body, body_text);

  return signcrypt;
}

int message_jabber_client_handler(xmpp_conn_t *const conn,
                                  xmpp_stanza_t *const stanza,
                                  void *const userdata) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "message_jabber_client_handler");
  eagle_ctx_t *eagle = (eagle_ctx_t *)userdata;
  xmpp_stanza_t *message = stanza;

  // XEP-0280: Message Carbons
  xmpp_stanza_t *carbon =
      xmpp_stanza_get_child_by_ns(stanza, EAGLE_XMPP_NS_CARBONS_2);
  if (carbon) {
    message = NULL;
    xmpp_stanza_t *forwarded =
        xmpp_stanza_get_child_by_ns(carbon, EAGLE_XMPP_NS_FORWARD);
    if (forwarded) {
      message = xmpp_stanza_get_child_by_name(forwarded,
                                              EAGLE_XMPP_STANZA_NAME_MESSAGE);
    }
  }
  // Message
  if (message && xmpp_message_get_body(message) != NULL) {
    GString *from = g_string_new(xmpp_stanza_get_from(message));
    g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Message from: %s ", from->str);
    GString *body_text = g_string_new(xmpp_message_get_body(message));
    eagle_message_t *m = eagle_xmpp_message_new();
    eagle_xmpp_adr_t *from_adr = eagle_xmpp_adr_new_parse(from);
    m->from = from_adr;
    m->message_text = body_text;
    eagle_add_message(eagle, m);
  }
  return TRUE; // keep
}

int message_headline_handler(xmpp_conn_t *const conn,
                             xmpp_stanza_t *const stanza,
                             void *const userdata) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Incoming Headline Message");
  eagle_ctx_t *eagle = (eagle_ctx_t *)userdata;

  xmpp_stanza_t *entry = NULL;

  xmpp_stanza_t *a = NULL;
  xmpp_stanza_t *event =
      xmpp_stanza_get_child_by_name(stanza, EAGLE_XMPP_STANZA_NAME_EVENT);
  if (event != NULL &&
      strcmp(xmpp_stanza_get_ns(event), EAGLE_XMPP_NS_PUBSUB_EVENT) == 0) {
    xmpp_stanza_t *items =
        xmpp_stanza_get_child_by_name(event, EAGLE_XMPP_STANZA_NAME_ITEMS);
    if (items != NULL && strcmp(EAGLE_XMPP_NS_MICROBLOG_0,
                                xmpp_stanza_get_attribute(
                                    items, EAGLE_XMPP_STANZA_ATT_NODE)) == 0) {
      g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "urn:xmpp:microblog:0 items found!");
      a = items;
      xmpp_stanza_t *i =
          xmpp_stanza_get_child_by_name(items, EAGLE_XMPP_STANZA_NAME_ITEM);
      if (i) {
        entry = xmpp_stanza_get_child_by_name(i, EAGLE_XMPP_STANZA_NAME_ENTRY);
        if (entry) {
          xmpp_stanza_t *content = xmpp_stanza_get_child_by_name(
              entry, EAGLE_XMPP_STANZA_NAME_CONTENT);
          a = content;
        }
      }
    }
  }

  if (a) {
    GString *from = g_string_new(xmpp_stanza_get_from(stanza));
    xmpp_stanza_t *title = xmpp_stanza_get_child_by_name(entry, "title");
    xmpp_stanza_t *updated = xmpp_stanza_get_child_by_name(entry, "updated");

    char *buf;
    size_t buflen;
    xmpp_stanza_to_text(a, &buf, &buflen);
    eagle_blog_message_t *m = eagle_blog_message_new(
        from, g_string_new(xmpp_stanza_get_text(updated)),
        g_string_new(xmpp_stanza_get_text(title)));
    eagle_add_blog_message(eagle, m);
  }
  return TRUE; // keep handler
}

int _incoming_iq(xmpp_conn_t *const conn, xmpp_stanza_t *const stanza,
                 void *const userdata) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Incomming IQ");

  // XEP-0092: Software Version
  xmpp_stanza_t *query = xmpp_stanza_get_child_by_name_and_ns(
      stanza, "query", "jabber:iq:version");
  if (query) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO,
          "Request for XEP-0092: Software Version");
    const char *from = xmpp_stanza_get_from(stanza);
    const char *id = xmpp_stanza_get_id(stanza);
    xmpp_stanza_t *const iq_result =
        xmpp_iq_new(xmpp_conn_get_context(conn), "result", id);
    xmpp_stanza_t *const query = xmpp_stanza_new(xmpp_conn_get_context(conn));
    xmpp_stanza_set_name(query, "query");
    xmpp_stanza_set_ns(query, "jabber:iq:version");
    xmpp_stanza_add_child(iq_result, query);
    xmpp_stanza_t *const name = xmpp_stanza_new(xmpp_conn_get_context(conn));
    xmpp_stanza_set_name(name, "name");
    xmpp_stanza_t *const name_text =
        xmpp_stanza_new(xmpp_conn_get_context(conn));
    xmpp_stanza_set_text(name_text, "XMPP eagle");
    xmpp_stanza_add_child(name, name_text);
    xmpp_stanza_t *const version = xmpp_stanza_new(xmpp_conn_get_context(conn));
    xmpp_stanza_set_name(version, "version");
    xmpp_stanza_t *const version_text =
        xmpp_stanza_new(xmpp_conn_get_context(conn));
    xmpp_stanza_set_text(version_text, "development");
    xmpp_stanza_add_child(version, version_text);
    xmpp_stanza_t *const os = xmpp_stanza_new(xmpp_conn_get_context(conn));
    xmpp_stanza_set_name(os, "os");
    xmpp_stanza_t *const os_text = xmpp_stanza_new(xmpp_conn_get_context(conn));
    xmpp_stanza_set_text(os_text, "GNU/Linux");
    xmpp_stanza_add_child(os, os_text);
    xmpp_stanza_add_child(query, name);
    xmpp_stanza_add_child(query, version);
    xmpp_stanza_add_child(query, os);
    xmpp_stanza_set_to(iq_result, from);
    xmpp_send(conn, iq_result);
    xmpp_stanza_release(iq_result);
  }

  return TRUE;
}

/*
<presence
    from='hag66@shakespeare.lit/pda'
    id='n13mt3l'
    to='coven@chat.shakespeare.lit/thirdwitch'>
  <x xmlns='http://jabber.org/protocol/muc'/>
</presence>
*/

void join_muc(const eagle_ctx_t *const eagle_ctx, xmpp_conn_t *const conn,
              const char *jid) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Joining MUC %s", jid);
  xmpp_stanza_t *const presence =
      xmpp_presence_new(xmpp_conn_get_context(conn));
  char *id = xmpp_uuid_gen(xmpp_conn_get_context(conn));
  xmpp_stanza_set_id(presence, id);
  xmpp_stanza_set_to(presence, jid);
  xmpp_stanza_t *x = xmpp_stanza_new(xmpp_conn_get_context(conn));
  xmpp_stanza_set_name(x, "x");
  xmpp_stanza_add_child(presence, x);
  xmpp_stanza_set_ns(x, "http://jabber.org/protocol/muc");
  xmpp_send(conn, presence);
  xmpp_stanza_release(presence);
}

static int _incoming_message_handler(xmpp_conn_t *const conn,
                                     xmpp_stanza_t *const stanza,
                                     void *const userdata) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Incomming message");
  GString *log_text = g_string_new("Incomming Message from ");
  const char *from = xmpp_stanza_get_from(stanza);
  log_text = g_string_append(log_text, from);

  const char *type = xmpp_stanza_get_type(stanza);
  if (!g_strcmp0(type, "chat")) {
    log_text = g_string_append(log_text, "chat");
  } else if (!g_strcmp0(type, "groupchat")) {
    // incoming group chat message
    g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Incoming group chat message");
    xmpp_stanza_t *body = xmpp_stanza_get_child_by_name(stanza, "body");

    if (body) {
      char *text = xmpp_stanza_get_text(body);
      log_text = g_string_append(log_text, text);
      eagle_xmpp_adr_t *from =
          eagle_xmpp_adr_new_parse(g_string_new(xmpp_stanza_get_from(stanza)));
      eagle_xmpp_adr_t *to =
          eagle_xmpp_adr_new_parse(g_string_new(xmpp_stanza_get_to(stanza)));
      eagle_message_t *m = eagle_xmpp_message_new();
      m->from = from;
      m->to = to;
      m->is_groupchat = TRUE;
      m->message_text = g_string_new(text);
      eagle_add_message(userdata, m);
    }
  }

  log_text = g_string_append(log_text, "\n");
  eagle_log(userdata, log_text);
  g_string_free(log_text, TRUE);
  return TRUE;
}

static int _incoming_presence_handler(xmpp_conn_t *const conn,
                                      xmpp_stanza_t *const stanza,
                                      void *const userdata) {

  eagle_presence_show_t presence_show = EAGLE_PRESENCE_UNKOWN;
  const char *stanza_from = xmpp_stanza_get_from(stanza);
  eagle_xmpp_adr_t *from = eagle_xmpp_adr_new_parse(g_string_new(stanza_from));

  // https://tools.ietf.org/html/rfc6121#section-4.7.1
  // Mapping of presence type to enum eagle_presence_type_t
  const char *stanza_type = xmpp_stanza_get_type(stanza);
  eagle_presence_type_t presence_type = EAGLE_PRESENCE_AVAILABLE;
  if (!g_strcmp0(stanza_type, "error"))
    presence_type = EAGLE_PRESENCE_ERROR;
  else if (!g_strcmp0(stanza_type, "subscribe"))
    presence_type = EAGLE_PRESENCE_SUBSCRIBE;
  else if (!g_strcmp0(stanza_type, "subscribed"))
    presence_type = EAGLE_PRESENCE_SUBSCRIBED;
  else if (!g_strcmp0(stanza_type, "unavailable"))
    presence_type = EAGLE_PRESENCE_UNAVAILABLE;
  else if (!g_strcmp0(stanza_type, "unsubscribe"))
    presence_type = EAGLE_PRESENCE_UNSUBSCRIBE;
  else if (!g_strcmp0(stanza_type, "unsubscribed"))
    presence_type = EAGLE_PRESENCE_UNSUBSCRIBED;

  GString *status_text = NULL;
  xmpp_stanza_t *status = xmpp_stanza_get_child_by_name(stanza, "status");
  if (status) {
    status_text = g_string_new(xmpp_stanza_get_text(status));
  }

  // https://tools.ietf.org/html/rfc6121#section-4.5 - Unavailable Presence
  if (presence_type == EAGLE_PRESENCE_UNAVAILABLE) {
    // unavailable -- The sender is no longer available for communication.
    presence_show = EAGLE_PRESENCE_UNKOWN;
    eagle_presence_t *presence =
        eagle_presence_new(from, presence_type, presence_show, status_text);
    eagle_log_presence(userdata, presence);
    return TRUE;
  }

  // https://tools.ietf.org/html/rfc6121#section-3.1 - Requesting a Subscription
  if (presence_type == EAGLE_PRESENCE_SUBSCRIBE) {
    // subscribe -- The sender wishes to subscribe to the recipient's presence.
  }

  presence_show = EAGLE_PRESENCE_ONLINE;
  xmpp_stanza_t *show = xmpp_stanza_get_child_by_name(stanza, "show");
  if (show) {
    char *show_text = xmpp_stanza_get_text(show);
    if (!g_strcmp0(show_text, "away"))
      presence_show = EAGLE_PRESENCE_AWAY;
    else if (!g_strcmp0(show_text, "chat"))
      presence_show = EAGLE_PRESENCE_CHAT;
    else if (!g_strcmp0(show_text, "dnd"))
      presence_show = EAGLE_PRESENCE_DND;
    else if (!g_strcmp0(show_text, "xa"))
      presence_show = EAGLE_PRESENCE_XA;
  }

  eagle_presence_t *presence =
      eagle_presence_new(from, presence_type, presence_show, status_text);
  eagle_log_presence(userdata, presence);
  return TRUE;

  /*

  GString* log_text = g_string_new("Incomming Presence from ");
  const char *from = xmpp_stanza_get_from(stanza);
  log_text = g_string_append(log_text, from);

  xmpp_stanza_t *idle = xmpp_stanza_get_child_by_name_and_ns(stanza, "idle",
  "urn:xmpp:idle:1"); if ( idle ) { const char* since =
  xmpp_stanza_get_attribute(idle, "since"); log_text = g_string_append(log_text,
  "\tseit "); log_text = g_string_append(log_text, since);
  }
  xmpp_stanza_t *show = xmpp_stanza_get_child_by_name(stanza, "show");
  if( show ) {
    char* show_text = xmpp_stanza_get_text(show);
    log_text = g_string_append(log_text, "\t<b>Status:</b> ");
    log_text = g_string_append(log_text, show_text);
  }
  xmpp_stanza_t *status = xmpp_stanza_get_child_by_name(stanza, "status");
  if( status ) {
    char* status_text = xmpp_stanza_get_text(status);
    log_text = g_string_append(log_text, status_text);
  }

  log_text = g_string_append(log_text, "\n");
  eagle_log(userdata, log_text);
  g_string_free(log_text, TRUE);
  */
  return TRUE;
}
