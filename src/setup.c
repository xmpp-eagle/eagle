/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file setup.c
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#include "setup.h"

#define LOG_DOMAIN "eagle-setup"

#define EAGLE_CONFIG_FILE "eagle.conf"

#define SETUP_XMPP "xmpp"
#define SETUP_XMPP_JID "jid"
#define SETUP_XMPP_PWD "pwd"
#define SETUP_XMPP_TLS_TRUST "tls.trust"

#define SETUP_GNUPG "gnupg"
#define SETUP_GNUPG_HOMEDIR "homedir"

#define SETUP_ABOOK "abook"
#define SETUP_ABOOK_HOMEDIR "homedir"

#define SETUP_VCARD "vcard"
#define SETUP_VCARD_HOMEDIR "homedir"

static GKeyFile *_get_config_key_file(const char *file);
static gboolean _get_boolean_gek_file_value(GKeyFile *key_file,
                                            const gchar *group_name,
                                            const gchar *key, GError **error);

eagle_error_code_t setup_load_config(eagle_ctx_t *eagle_ctx,
                                     eagle_settings_t **settings,
                                     eagle_account_t **account) {
  *settings = malloc(sizeof(eagle_settings_t));
  *account = malloc(sizeof(eagle_account_t));

  (*account)->jid = NULL;
  (*account)->pwd = NULL;
  (*account)->tls_trust = FALSE;
  (*account)->xmpp_auto_connect = FALSE;
  (*settings)->gnupg_homedir = NULL;

#ifdef PACKAGE_STATUS_DEVELOPMENT
  const char *user_data_dir = "./dev/user_data";
  (*settings)->gnupg_homedir = g_string_new("./dev/gnupg");
#else
  const char *user_data_dir = g_get_user_data_dir();
#endif

  GString *local_homedir = g_string_new(user_data_dir);
  g_string_append(local_homedir, "/eagle/abook/");
  (*settings)->local_homedir = local_homedir;

  GString *vcard_homedir = g_string_new(user_data_dir);
  g_string_append(vcard_homedir, "/eagle/vcards/");
  (*settings)->vcard_homedir = vcard_homedir;

  GKeyFile *config_file = _get_config_key_file(EAGLE_CONFIG_FILE);
  if (config_file) {

    // Loading configs
    GError *error = NULL;

    // XMPP Setup
    GString *jid = g_string_new(
        g_key_file_get_value(config_file, SETUP_XMPP, SETUP_XMPP_JID, &error));
    if (!error)
      (*account)->jid = jid;
    error = NULL;
    GString *pwd = g_string_new(
        g_key_file_get_value(config_file, SETUP_XMPP, SETUP_XMPP_PWD, &error));
    if (!error)
      (*account)->pwd = pwd;
    error = NULL;
    gboolean tls_trust = _get_boolean_gek_file_value(
        config_file, SETUP_XMPP, SETUP_XMPP_TLS_TRUST, &error);
    if (!error)
      (*account)->tls_trust = tls_trust;
    error = NULL;
    gboolean xmpp_auto_connect = _get_boolean_gek_file_value(
        config_file, SETUP_XMPP, "xmpp_auto_connect", &error);
    (*account)->xmpp_auto_connect = xmpp_auto_connect;
    // GnuPG Setup
    error = NULL;
    GString *gnupg_homedir = g_string_new(g_key_file_get_value(
        config_file, SETUP_GNUPG, SETUP_GNUPG_HOMEDIR, &error));
    if (!error)
      (*settings)->gnupg_homedir = gnupg_homedir;
    // Local Setup
    error = NULL;
    GString *local_homedir = g_string_new(g_key_file_get_value(
        config_file, SETUP_ABOOK, SETUP_ABOOK_HOMEDIR, &error));
    if (!error)
      (*settings)->local_homedir = local_homedir;
    //
    error = NULL;
    GString *vcard_homedir = g_string_new(g_key_file_get_value(
        config_file, SETUP_VCARD, SETUP_VCARD_HOMEDIR, &error));
    if (!error)
      (*settings)->vcard_homedir = vcard_homedir;
  }

  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Setup - tls_trust:\t%d",
        (*account)->tls_trust);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Setup - xmpp_auto_connect:\t%d",
        (*account)->xmpp_auto_connect);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Setup - local_homedir:\t%s",
        (*settings)->local_homedir->str);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Setup - vcard_homedir:\t%s",
        (*settings)->vcard_homedir->str);

  return EAGLE_ERR_NO_ERROR;
}

static GKeyFile *_get_config_key_file(const char *file) {
  GKeyFile *config_file = g_key_file_new();
  GError *error = NULL;
  // XDG_CONFIG_HOME

#ifdef PACKAGE_STATUS_DEVELOPMENT
  const char *user_config_dir = "./dev/user_config";
#else
  const char *user_config_dir = g_get_user_config_dir();
#endif

  GString *config_path = g_string_new(user_config_dir);
  g_string_append(config_path, "/");
  g_string_append(config_path, file);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Loading settings from %s",
        config_path->str);
  gboolean loaded = g_key_file_load_from_file(config_file, config_path->str,
                                              G_KEY_FILE_NONE, &error);
  if (!loaded) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_WARNING,
          "Configuration file %s not loaded: %s", config_path->str,
          error->message);
    return NULL;
  }
  return config_file;
}

gboolean _get_boolean_gek_file_value(GKeyFile *key_file,
                                     const gchar *group_name, const gchar *key,
                                     GError **error) {
  g_assert(key_file && group_name && key);
  gboolean result = FALSE;
  char *flag = NULL;
  flag = g_key_file_get_value(key_file, group_name, key, error);

  if (!(*error) && flag) {
    if (!g_strcmp0("true", flag)) {
      result = TRUE;
    } else {
      result = FALSE;
    }
  }
  return result;
}
