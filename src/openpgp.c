/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file openpgp.c
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#include <locale.h>
#include <openpgp.h>

#define LOG_DOMAIN "eagle-openpgp"

gpgme_user_id_t _openpgp_lookup_xmpp_user(gpgme_key_t key);

gpgme_error_t openpgp_init_gpgme(gpgme_ctx_t *gpgme_ctx,
                                 eagle_settings_t *settings) {
  g_assert(*gpgme_ctx == NULL);
  setlocale(LC_ALL, "");
  gpgme_check_version(NULL);
  gpgme_set_locale(NULL, LC_CTYPE, setlocale(LC_CTYPE, NULL));

  gpgme_error_t error = gpgme_new(gpgme_ctx);
  if (GPG_ERR_NO_ERROR != error) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_ERROR, "GpgME Error - gpgme_new: %s %s",
          gpgme_strsource(error), gpgme_strerror(error));
    return error;
  }

  error = gpgme_set_protocol(*gpgme_ctx, GPGME_PROTOCOL_OPENPGP);
  if (error != GPG_ERR_NO_ERROR) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_ERROR, "GpgME Error: gpgme_set_protocol - %s",
          gpgme_strerror(error));
    return error;
  }

  gpgme_engine_info_t engine_info = gpgme_ctx_get_engine_info(*gpgme_ctx);
  if (settings->gnupg_homedir) {
    error = gpgme_ctx_set_engine_info(*gpgme_ctx, GPGME_PROTOCOL_OPENPGP,
                                      engine_info->file_name,
                                      settings->gnupg_homedir->str);
  }
  engine_info = gpgme_ctx_get_engine_info(*gpgme_ctx);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Homedir: %s", engine_info->home_dir);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Protocol: %s",
        gpgme_get_protocol_name(engine_info->protocol));
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Filename: %s", engine_info->file_name);

  gpgme_set_armor(*gpgme_ctx, 0);
  gpgme_set_textmode(*gpgme_ctx, 0);
  gpgme_set_offline(*gpgme_ctx, 1);
  gpgme_set_keylist_mode(*gpgme_ctx, GPGME_KEYLIST_MODE_LOCAL |
                                         GPGME_KEYLIST_MODE_SIGS |
                                         GPGME_KEYLIST_MODE_SIG_NOTATIONS);
  return error;
}

gpgme_error_t openpgp_keylist_primary_userid(gpgme_ctx_t gpgme_ctx,
                                             GList **list) {
  gpgme_error_t error = GPG_ERR_NO_ERROR;

  error = gpgme_op_keylist_start(gpgme_ctx, NULL, FALSE);
  if (error)
    return error;

  while (!error) {
    gpgme_key_t key;
    error = gpgme_op_keylist_next(gpgme_ctx, &key);
    if (!error) {
      eagle_contact_entity_t *entity = malloc(sizeof(eagle_contact_entity_t));
      entity->id = g_string_new(key->fpr);
      entity->name = g_string_new(key->uids ? key->uids->name : "");
      entity->email =
          g_string_new(key->uids && key->uids->email ? key->uids->email : "");
      entity->fingerprint = g_string_new(key->fpr);
      gpgme_user_id_t xmpp = _openpgp_lookup_xmpp_user(key);
      entity->is_ok =
          (!key->revoked && !key->expired && !key->disabled && !key->invalid);
      time_t expires = key->subkeys->expires;
      GDateTime *dt_expires = g_date_time_new_from_unix_utc(expires);
      entity->expire = g_string_new(g_date_time_format(dt_expires, "%FT%T"));
      if (xmpp)
        entity->xmpp = g_string_new(xmpp->name);
      else
        entity->xmpp = NULL;
      *list = g_list_append(*list, entity);
      gpgme_key_release(key);
    }
  }
  return error;
}

gpgme_error_t load_key_details(gpgme_ctx_t gpgme_ctx, char *keyid,
                               gpgme_key_t *key) {
  return gpgme_get_key(gpgme_ctx, keyid, key, 0);
}

gpgme_user_id_t _openpgp_lookup_xmpp_user(gpgme_key_t key) {
  gpgme_user_id_t uid = key->uids;
  while (uid) {
    if (uid->name && strstr(uid->name, "xmpp") == uid->name) {
      g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Found XMPP: %s", uid->name);
      return uid;
    }
    uid = uid->next;
  }
  return NULL;
}

gpgme_error_t openpgp_lookup_key(gpgme_ctx_t ctx, char *name,
                                 gpgme_key_t *key) {
  gpgme_error_t error = gpgme_op_keylist_start(ctx, NULL, 0);
  while (!error) {
    error = gpgme_op_keylist_next(ctx, key);
    if (!error) {
      gpgme_user_id_t uids = (*key)->uids;
      while (uids) {
        if (strcmp(uids->name, name) == 0) {
          g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Found Key: %s", uids->name);
          return error;
        }
        uids = uids->next;
      }
    } else {
      gpgme_key_release((*key));
    }
  }
  return error;
}

char *openpgp_signcrypt(gpgme_ctx_t ctx, gpgme_key_t from,
                        gpgme_key_t recipient, char *message) {
  g_assert(ctx);
  g_assert(from);
  g_assert(recipient);
  g_assert(message);

  gpgme_set_armor(ctx, 0);
  gpgme_set_textmode(ctx, 0);
  gpgme_set_offline(ctx, 1);
  gpgme_set_keylist_mode(ctx, GPGME_KEYLIST_MODE_LOCAL);
  gpgme_error_t error;
  gpgme_key_t recp[3];
  recp[0] = from, recp[1] = recipient;
  recp[2] = NULL;

  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "openpgp_signcrypt from %s to %s ",
        from->fpr, recipient->fpr);

  gpgme_signers_clear(ctx);
  error = gpgme_signers_add(ctx, recp[0]);
  if (error != 0) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_ERROR,
          "GpgME failed - gpgme_signers_add: %s %s: ", gpgme_strsource(error),
          gpgme_strerror(error));
    return NULL;
  }

  gpgme_encrypt_flags_t flags = 0;

  gpgme_data_t plain;
  gpgme_data_t cipher;

  error = gpgme_data_new(&plain);
  if (error != 0) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_ERROR,
          "GpgME failed - gpgme_data_new: %s %s: ", gpgme_strsource(error),
          gpgme_strerror(error));
    return NULL;
  }

  error = gpgme_data_new_from_mem(&plain, message, strlen(message), 0);
  if (error != 0) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_ERROR,
          "GpgME failed - gpgme_data_new_from_mem: %s %s: ",
          gpgme_strsource(error), gpgme_strerror(error));
    return NULL;
  }
  error = gpgme_data_new(&cipher);
  if (error != 0) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_ERROR,
          "GpgME failed - gpgme_data_new: %s %s: ", gpgme_strsource(error),
          gpgme_strerror(error));
    return NULL;
  }

  error = gpgme_op_encrypt_sign(ctx, recp, flags, plain, cipher);
  if (error != 0) {
    GString *error_str = g_string_new(gpgme_strerror(error));
    g_string_append(error_str, gpgme_strsource(error));
    return NULL;
  }

  size_t len;
  char *cipher_str = gpgme_data_release_and_get_mem(cipher, &len);
  char *result = g_base64_encode((unsigned char *)cipher_str, len);
  return result;
}
