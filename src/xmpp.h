/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file xmpp.h
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#ifndef EAGLE_XMPP_H__
#define EAGLE_XMPP_H__

#include "eagle.h"
#include <gpgme.h>
#include <strophe.h>

#define EAGLE_XMPP_TYPE_SET "set"
#define EAGLE_XMPP_TYPE_GET "get"
#define EAGLE_XMPP_TYPE_ERROR "error"
#define EAGLE_XMPP_TYPE_CHAT "chat"
#define EAGLE_XMPP_TYPE_GROUPCHAT "groupchat"
#define EAGLE_XMPP_TYPE_HEADLINE "headline"

#define EAGLE_XMPP_NS_HEADLINE "headline"
#define EAGLE_XMPP_NS_CARBONS_2 "urn:xmpp:carbons:2"
#define EAGLE_XMPP_NS_JABBER_CLIENT "jabber:client"
#define EAGLE_XMPP_NS_MICROBLOG_0 "urn:xmpp:microblog:0"
#define EAGLE_XMPP_NS_PUBSUB_EVENT "http://jabber.org/protocol/pubsub#event"
#define EAGLE_XMPP_NS_FORWARD "urn:xmpp:forward:0"

#define EAGLE_XMPP_STANZA_NAME_MESSAGE "message"
#define EAGLE_XMPP_STANZA_NAME_QUERY "query"
#define EAGLE_XMPP_STANZA_NAME_EVENT "event"
#define EAGLE_XMPP_STANZA_NAME_ITEMS "items"
#define EAGLE_XMPP_STANZA_NAME_ITEM "item"
#define EAGLE_XMPP_STANZA_NAME_ENTRY "entry"
#define EAGLE_XMPP_STANZA_NAME_CONTENT "content"
#define EAGLE_XMPP_STANZA_NAME_BODY "body"

#define EAGLE_XMPP_STANZA_ATT_NAME "name"
#define EAGLE_XMPP_STANZA_ATT_NODE "node"
#define EAGLE_XMPP_STANZA_ATT_JID "jid"
#define EAGLE_XMPP_STANZA_ATT_SUBSCRIPTION "subscription"

void setup_iq_handler(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn);

void send_presence(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn);

void send_carbons(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn);

void request_bookmarks(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn);

void request_roster(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn,
                    eagle_roster_t *roster);
void xmpp_send_openpgp_message(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn,
                               gpgme_ctx_t gpgme_ctx, GString *to,
                               GString *text);

void xmpp_send_message(eagle_ctx_t *eagle_ctx, xmpp_conn_t *conn,
                       const char *type, GString *to, GString *text);

int message_jabber_client_handler(xmpp_conn_t *const conn,
                                  xmpp_stanza_t *const stanza,
                                  void *const userdata);

int message_headline_handler(xmpp_conn_t *const conn,
                             xmpp_stanza_t *const stanza, void *const userdata);

void join_muc(const eagle_ctx_t *const eagle_ctx, xmpp_conn_t *const conn,
              const char *jid);

#endif // EAGLE_XMPP_H__
