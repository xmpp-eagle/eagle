/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file core.c
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle. If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#include "core.h"
#include <gpgme.h>
#include <strophe.h>

#include "abook.h"
#include "openpgp.h"
#include "setup.h"
#include "ui/ui_ctrl_eagle.h"
#include "ui/ui_ctrl_groupchat.h"
#include "ui/ui_groupchat.h"
#include "vcard.h"
#include "xmpp.h"

#define LOG_DOMAIN "eagle-core"
#define LOG_DOMAIN_TRIGGER "eagle-core-trigger"

#define TIMER_BACKGROUD_TASKS 1000

/*!
 * ticking interval of XMPP (milliseconds)
 */
#define TIMER_XMPP 100
/*!
 *  Timeout for XMPP (milliseconds)
 */

#define XMPP_TIMEOUT 10

/*!
 * eagle context structure
 *
 */
struct _eagle_ctx_t {
  GtkBuilder *builder;
  gpgme_ctx_t gpgme_ctx;
  eagle_settings_t *settings;
  eagle_account_t *account;
  xmpp_ctx_t *xmpp_ctx;
  xmpp_conn_t *xmpp_conn;
  eagle_roster_t *roster;
  eagle_addressbook_t *vcards;
  eagle_addressbook_t *abook;
  GList *messages;
  GList *blog_messages;
  GList *bookmark_conference; // eagle_bookmark_conference_t
  GList *bookmark_url;        // eagle_bookmark_url_t
  /*! List of eagle_groupchat_t* **/
  GList *multiuserchat;
  GHashTable *builder_hash_groupchat; // GtkBuilder
};

static gboolean trigger_xmpp(gpointer data);

static gboolean trigger_background_tasks(gpointer data);

static eagle_error_code_t _eagle_xmpp_connect(eagle_ctx_t *eagle_ctx);

static void _connection_handler(xmpp_conn_t *const conn,
                                const xmpp_conn_event_t status, const int error,
                                xmpp_stream_error_t *const stream_error,
                                void *const userdata);

static void _refresh_ui(eagle_ctx_t *eagle_ctx);

static eagle_groupchat_t *
_lookup_groupchat(eagle_ctx_t *eagle_ctx,
                  const eagle_xmpp_adr_t *const chatgroup);

eagle_ctx_t *eagle_ctx_new(GtkBuilder *builder) {
  eagle_ctx_t *eagle_ctx = malloc(sizeof(eagle_ctx_t));
  eagle_ctx->builder = builder;
  eagle_ctx->settings = NULL;
  eagle_ctx->account = NULL;
  eagle_ctx->gpgme_ctx = NULL;
  eagle_ctx->xmpp_ctx = NULL;
  eagle_ctx->xmpp_conn = NULL;
  eagle_ctx->roster = NULL;
  eagle_ctx->vcards = NULL;
  eagle_ctx->abook = NULL;
  eagle_ctx->messages = NULL;
  eagle_ctx->blog_messages = NULL;
  eagle_ctx->bookmark_conference = NULL;
  eagle_ctx->bookmark_url = NULL;
  eagle_ctx->multiuserchat = NULL;
  eagle_ctx->builder_hash_groupchat = g_hash_table_new(NULL, NULL);
  return eagle_ctx;
}

eagle_error_code_t eagle_init(eagle_ctx_t *eagle_ctx) {
  eagle_error_code_t error = EAGLE_ERR_NO_ERROR;

  error =
      setup_load_config(eagle_ctx, &eagle_ctx->settings, &eagle_ctx->account);

  eagle_addressbook_t *local = malloc(sizeof(eagle_addressbook_t));
  local->list = NULL;
  error = load_abook_adressbook(eagle_ctx, eagle_ctx->settings, local);
  set_addressbook_abook(eagle_ctx, eagle_ctx->builder, local->list);

  eagle_addressbook_t *vcard = malloc(sizeof(eagle_addressbook_t));
  vcard->list = NULL;
  vcard_init(eagle_ctx->settings, vcard);
  eagle_ctx->vcards = vcard;
  set_addressbook_vcard(eagle_ctx, eagle_ctx->builder, vcard->list);

  gpgme_error_t gpgme_error =
      openpgp_init_gpgme(&(eagle_ctx->gpgme_ctx), eagle_ctx->settings);
  if (gpgme_error != GPG_ERR_NO_ERROR) {
    error = EAGLE_ERR_INIT_GPGME;
  }
  GList *list = NULL;
  error = openpgp_keylist_primary_userid(eagle_ctx->gpgme_ctx, &list);
  if (list != NULL) {
    set_addressbook_openpgp(eagle_ctx, eagle_ctx->builder, list);
  }

  /* Automatically connect to XMPP Server during application start */
  if (eagle_ctx->account->xmpp_auto_connect) {
    _eagle_xmpp_connect(eagle_ctx);
  }

  g_timeout_add(TIMER_BACKGROUD_TASKS, trigger_background_tasks, eagle_ctx);

  return error;
}

eagle_error_code_t _eagle_xmpp_connect(eagle_ctx_t *eagle_ctx) {

  xmpp_connecting(eagle_ctx, eagle_ctx->builder);
  if (!eagle_ctx->account->jid) {
    return EAGLE_ERR_NO_ERROR;
  }

  // XMPP
  xmpp_log_t *logger = xmpp_get_default_logger(XMPP_LEVEL_DEBUG);
  eagle_ctx->xmpp_ctx = xmpp_ctx_new(NULL, logger);

  // XMMP Connection
  eagle_ctx->xmpp_conn = xmpp_conn_new(eagle_ctx->xmpp_ctx);

  xmpp_conn_set_jid(eagle_ctx->xmpp_conn, eagle_ctx->account->jid->str);
  xmpp_conn_set_pass(eagle_ctx->xmpp_conn, eagle_ctx->account->pwd->str);

  if (eagle_ctx->account->tls_trust == TRUE) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_WARNING, "Setting XMPP_CONN_FLAG_TRUST_TLS");
    xmpp_conn_set_flags(eagle_ctx->xmpp_conn, XMPP_CONN_FLAG_TRUST_TLS);
  } else {
    xmpp_conn_set_flags(eagle_ctx->xmpp_conn, XMPP_CONN_FLAG_MANDATORY_TLS);
  }
  g_timeout_add(TIMER_XMPP, trigger_xmpp, eagle_ctx);
  int error = xmpp_connect_client(eagle_ctx->xmpp_conn, NULL, 0,
                                  _connection_handler, eagle_ctx);
  if (error != XMPP_EOK) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_WARNING, "XMPP Connection failed: %d", error);
  }
  return EAGLE_ERR_NO_ERROR;
}

void _connection_handler(xmpp_conn_t *const conn,
                         const xmpp_conn_event_t status, const int error,
                         xmpp_stream_error_t *const stream_error,
                         void *const userdata) {
  eagle_ctx_t *eagle_ctx = (eagle_ctx_t *)userdata;
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "_connection_handler");

  if (error != 0) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_WARNING, "Connection failed. %s",
          strerror(error));
    xmpp_stop(xmpp_conn_get_context(conn));
    return;
  }

  if (stream_error != NULL) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_WARNING, "Connection failed. %s",
          stream_error->text);
    xmpp_stop(xmpp_conn_get_context(conn));
    return;
  }

  switch (status) {
  case XMPP_CONN_CONNECT:
    g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "XMPP connected");
    setup_iq_handler(eagle_ctx, eagle_ctx->xmpp_conn);
    xmpp_connected(eagle_ctx, eagle_ctx->builder);
    eagle_roster_t *roster = malloc(sizeof(eagle_roster_t));
    roster->list = NULL;
    roster->updated = FALSE;
    eagle_ctx->roster = roster;
    request_roster(eagle_ctx, eagle_ctx->xmpp_conn, eagle_ctx->roster);
    request_bookmarks(eagle_ctx, eagle_ctx->xmpp_conn);

    xmpp_handler_add(
        conn, message_jabber_client_handler, EAGLE_XMPP_NS_JABBER_CLIENT,
        EAGLE_XMPP_STANZA_NAME_MESSAGE, EAGLE_XMPP_TYPE_CHAT, eagle_ctx);

    xmpp_handler_add(conn, message_headline_handler, NULL,
                     EAGLE_XMPP_STANZA_NAME_MESSAGE, EAGLE_XMPP_TYPE_HEADLINE,
                     eagle_ctx);

    send_presence(eagle_ctx, eagle_ctx->xmpp_conn);
    send_carbons(eagle_ctx, eagle_ctx->xmpp_conn);

    break;
  case XMPP_CONN_RAW_CONNECT:
    g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "XMPP RAW connect");
    break;
  case XMPP_CONN_DISCONNECT:
    xmpp_disconnect(conn);
    xmpp_stop(xmpp_conn_get_context(conn));
    g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "XMPP disconnected");
    break;
  case XMPP_CONN_FAIL:
    g_log(LOG_DOMAIN, G_LOG_LEVEL_WARNING, "XMPP connection failed");
    xmpp_stop(xmpp_conn_get_context(conn));
    break;
  }
}

gboolean trigger_xmpp(gpointer data) {
  g_log(LOG_DOMAIN_TRIGGER, G_LOG_LEVEL_DEBUG, "Ticking trigger_xmpp");
  g_assert(data);
  eagle_ctx_t *eagle_ctx = data;
  xmpp_run_once(eagle_ctx->xmpp_ctx, XMPP_TIMEOUT);
  return TRUE;
}

gboolean trigger_background_tasks(gpointer data) {
  g_log(LOG_DOMAIN_TRIGGER, G_LOG_LEVEL_DEBUG,
        "Ticking trigger_background_tasks");
  eagle_ctx_t *eagle_ctx = data;
  _refresh_ui(eagle_ctx);
  return TRUE;
}

static void _refresh_ui(eagle_ctx_t *eagle_ctx) {
  if (eagle_ctx->roster && eagle_ctx->roster->updated) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Update roster");
    set_addressbook_xmpp(eagle_ctx, eagle_ctx->builder,
                         eagle_ctx->roster->list);
    eagle_ctx->roster->updated = FALSE;
  }
}

void eagle_show_openpgp_details(eagle_ctx_t *eagle_ctx, char *openpgpkey) {
  GtkBuilder *builder = gtk_builder_new_from_file(EAGLE_UI_FILE_OPENPGP);
  GObject *dialog = gtk_builder_get_object(builder, "DialogOpenPGP");

  dialog_openpgp_key_t *dialog_openpgp = malloc(sizeof(dialog_openpgp_key_t));
  dialog_openpgp->builder = builder;
  dialog_openpgp->keyid = g_string_new(openpgpkey);
  dialog_openpgp->eagle_ctx = eagle_ctx;
  dialog_openpgp->key = NULL;

  load_key_details(eagle_ctx->gpgme_ctx, openpgpkey, &dialog_openpgp->key);

  gtk_builder_connect_signals(builder, dialog_openpgp);
  gtk_widget_show(GTK_WIDGET(dialog));
}

void eagle_show_new_message(eagle_ctx_t *eagle_ctx, char *xmppid) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Open Message for %s", xmppid);
  g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Builder %s", EAGLE_UI_FILE_MESSAGE);

  eagle_message_ctx_t *eagle_message_ctx = malloc(sizeof(eagle_message_ctx_t));

  eagle_message_ctx->eagle_ctx = eagle_ctx;
  eagle_message_ctx->xmppid = g_string_new(xmppid);

  GtkBuilder *builder = gtk_builder_new_from_file(EAGLE_UI_FILE_MESSAGE);
  eagle_message_ctx->builder = builder;
  g_assert(builder);
  gtk_builder_connect_signals(builder, eagle_message_ctx);
  GtkDialog *dialog =
      GTK_DIALOG(gtk_builder_get_object(builder, "DialogMessageCompose"));
  GtkToolButton *user =
      GTK_TOOL_BUTTON(gtk_builder_get_object(builder, "ToolButtonUser"));
  gtk_tool_button_set_label(user, xmppid);

  g_assert(dialog);

  gtk_dialog_run(dialog);
}

void eagle_send_openpgp_message(eagle_ctx_t *eagle_ctx, GString *to,
                                GString *text) {
  xmpp_send_openpgp_message(eagle_ctx, eagle_ctx->xmpp_conn,
                            eagle_ctx->gpgme_ctx, to, text);
}

void eagle_send_message(eagle_ctx_t *eagle_ctx, message_type_t type,
                        GString *to, GString *text) {
  switch (type) {
  case NORMAL:
    xmpp_send_message(eagle_ctx, eagle_ctx->xmpp_conn, "normal", to, text);
    break;
  case CHAT:
    xmpp_send_message(eagle_ctx, eagle_ctx->xmpp_conn, "chat", to, text);
    break;
  case GROUPCHAT:
    xmpp_send_message(eagle_ctx, eagle_ctx->xmpp_conn, "groupchat", to, text);
    break;
  }
}

eagle_message_t *eagle_message_new(GString *from, GString *message_text) {
  eagle_message_t *message = malloc(sizeof(eagle_message_t));
  message->from = eagle_xmpp_adr_new_parse(from);
  message->message_text = message_text;
  return message;
}

void eagle_add_message(eagle_ctx_t *eagle, eagle_message_t *message) {
  if (message->is_groupchat) {
    g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Group message");
    eagle_groupchat_t *chat = _lookup_groupchat(eagle, message->from);
    GtkBuilder *builder =
        g_hash_table_lookup(eagle->builder_hash_groupchat, chat);
    ui_groupchat_incoming_message(eagle, builder, message);
    if (chat) {
      g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Chat for group message found");
      chat->message_list = g_list_append(chat->message_list, message);
    }
  } else {
    eagle->messages = g_list_append(eagle->messages, message);
    message_add(eagle, eagle->builder, message);
  }
}

eagle_blog_message_t *eagle_blog_message_new(GString *from, GString *date,
                                             GString *title) {
  eagle_blog_message_t *message = malloc(sizeof(eagle_blog_message_t));
  message->from = eagle_xmpp_adr_new_parse(from);
  message->date = date;
  message->title = title;
  return message;
}

void eagle_add_blog_message(eagle_ctx_t *eagle, eagle_blog_message_t *message) {
  eagle->messages = g_list_append(eagle->blog_messages, message);
  blog_message_add(eagle, eagle->builder, message);
}

void eagle_add_conference_bookmark(
    eagle_ctx_t *eagle, eagle_bookmark_conference_t *conference_bookmark) {
  eagle->bookmark_conference =
      g_list_append(eagle->bookmark_conference, conference_bookmark);
  if (conference_bookmark->autojoin) {
    eagle_groupchat_join(eagle,
                         eagle_xmpp_adr_new_parse(conference_bookmark->jid));
  }
}

void eagle_add_url_bookmark(eagle_ctx_t *eagle,
                            eagle_bookmark_url_t *url_bookmark) {
  eagle->bookmark_url = g_list_append(eagle->bookmark_url, url_bookmark);
}

void eagle_groupchat_join(eagle_ctx_t *eagle, eagle_xmpp_adr_t *fulladr) {
  eagle_groupchat_t *groupchat = eagle_xmpp_groupchat_new(fulladr);
  eagle->multiuserchat = g_list_append(eagle->multiuserchat, groupchat);
  eagle_xmpp_groupchat_join(eagle, eagle->xmpp_conn, groupchat);
  muc_add(eagle, eagle->builder, groupchat);
}

void eagle_log(eagle_ctx_t *eagle, GString *message) {
  log_add(eagle, eagle->builder, message);
}

void eagle_log_presence(eagle_ctx_t *eagle, eagle_presence_t *presence) {
  ui_log_presence(eagle, eagle->builder, presence);
}

void eagle_show_groupchat(eagle_ctx_t *eagle_ctx, eagle_xmpp_adr_t *chatgroup) {

  GList *lookup = eagle_ctx->multiuserchat;
  eagle_groupchat_t *chat = NULL;
  while (lookup) {
    chat = lookup->data;
    if (eagle_xmpp_adr_compare(chat->address, chatgroup)) {
      g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Found chatgroup %s ",
            eagle_xmpp_adr_get_full(chat->address)->str);
      break;
    }
    lookup = g_list_next(lookup);
  }

  GtkBuilder *builder =
      g_hash_table_lookup(eagle_ctx->builder_hash_groupchat, chat);

  if (!builder) {
    builder = gtk_builder_new_from_file(EAGLE_UI_FILE_GROUPCHAT);
    g_hash_table_insert(eagle_ctx->builder_hash_groupchat, chat, builder);
  }
  GObject *dialog = gtk_builder_get_object(builder, "WindowChatWindow");

  // Create UI struct
  eagle_ui_groupchat_t *ui_groupchat = malloc(sizeof(eagle_ui_groupchat_t));
  ui_groupchat->eagle_ctx = eagle_ctx;
  ui_groupchat->builder = builder;
  ui_groupchat->groupchat = chat;

  gtk_builder_connect_signals(builder, ui_groupchat);
  gtk_widget_show(GTK_WIDGET(dialog));
}

static eagle_groupchat_t *
_lookup_groupchat(eagle_ctx_t *eagle_ctx,
                  const eagle_xmpp_adr_t *const chatgroup) {

  GList *lookup = eagle_ctx->multiuserchat;
  eagle_groupchat_t *chat = NULL;
  while (lookup) {
    chat = lookup->data;
    if (eagle_xmpp_adr_compare_bare(chat->address, chatgroup)) {
      g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Found chatgroup %s ",
            eagle_xmpp_adr_get_full(chat->address)->str);
      break;
    }
    lookup = g_list_next(lookup);
  }
  return chat;
}

void eagle_closed_groupchat(eagle_ctx_t *eagle_ctx,
                            eagle_xmpp_adr_t *chatgroup) {
  GList *lookup = eagle_ctx->multiuserchat;
  eagle_groupchat_t *chat = NULL;
  while (lookup) {
    chat = lookup->data;
    if (eagle_xmpp_adr_compare(chat->address, chatgroup)) {
      g_log(LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Found chatgroup %s ",
            eagle_xmpp_adr_get_full(chat->address)->str);
      break;
    }
    lookup = g_list_next(lookup);
  }
  g_hash_table_remove(eagle_ctx->builder_hash_groupchat, chat);
}
