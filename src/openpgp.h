/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file openpgp.h
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

#ifndef EAGLE_OPENPGP_H__
#define EAGLE_OPENPGP_H__

#ifdef PACKAGE_STATUS_DEVELOPMENT
#endif

#include <eagle.h>
#include <glib.h>
#include <gpgme.h>

gpgme_error_t openpgp_init_gpgme(gpgme_ctx_t *gpgme_ctx,
                                 eagle_settings_t *settings);

gpgme_error_t openpgp_keylist_primary_userid(gpgme_ctx_t gpgme_ctx, GList **);

gpgme_error_t load_key_details(gpgme_ctx_t gpgme_ctx, char *keyid,
                               gpgme_key_t *key);
gpgme_error_t openpgp_lookup_key(gpgme_ctx_t ctx, char *name, gpgme_key_t *key);

char *openpgp_signcrypt(gpgme_ctx_t ctx, gpgme_key_t from,
                        gpgme_key_t recipient, char *message);

#endif // EAGLE_OPENPGP_H__
