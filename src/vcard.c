/*!
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * @file vcard.h
 *
 * @authors
 * Copyright (C) 2020 DebXWoody <stefan@debxwoody.de>
 *
 * @copyright
 *
 * This file is part of eagle.
 *
 * eagle is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eagle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with eagle.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von eagle.
 *
 * eagle ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * eagle wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */

// https://tools.ietf.org/html/rfc6350

#include "vcard.h"

#include <vc.h>

#define LOG_DOMAIN "eagle-vcard"

static gboolean _isVCardFile(const char *filename);
static GString *_getStringValue(vc_component *vc, const char *name);

void vcard_init(eagle_settings_t *settings, eagle_addressbook_t *book) {
  g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Init vcard");
  g_assert(book->list == NULL);
  GString *path_vcard_dir = g_string_new(settings->vcard_homedir->str);

  GError *error = NULL;
  GDir *dir = g_dir_open(path_vcard_dir->str, 0, &error);
  if (dir == NULL) {
    return;
  }

  const char *f = g_dir_read_name(dir);
  while (f) {
    gboolean isVCardFile = _isVCardFile(f);
    if (isVCardFile) {
      GString *filename = g_string_new(path_vcard_dir->str);
      g_string_append(filename, "/");
      g_string_append(filename, f);
      g_log(LOG_DOMAIN, G_LOG_LEVEL_INFO, "Parsing VCard %s", filename->str);

      FILE *fp = fopen(filename->str, "r");

      if (fp) {
        vc_component *vcomponent = parse_vcard_file(fp);

        if (vcomponent) {
          vc_component *ventry = vcomponent;

          while (ventry) {
            eagle_contact_entity_t *person =
                malloc(sizeof(eagle_contact_entity_t));

            person->name = _getStringValue(ventry, VC_FORMATTED_NAME);
            person->lastname = _getStringValue(ventry, VC_NAME);
            // TODO: get_val_struct_part
            person->firstname = _getStringValue(ventry, VC_FORMATTED_NAME);
            person->email = _getStringValue(ventry, VC_EMAIL);
            person->phone = _getStringValue(ventry, VC_TELEPHONE);
            person->mobile = _getStringValue(ventry, VC_TELEPHONE);
            person->url = _getStringValue(ventry, VC_URL);
            person->notes = _getStringValue(ventry, VC_NOTE);

            book->list = g_list_append(book->list, person);
            ventry = parse_vcard_file(fp);
          }
        } else {
          g_log(LOG_DOMAIN, G_LOG_LEVEL_ERROR, "VCard read failed.");
        }
      } else {
        g_log(LOG_DOMAIN, G_LOG_LEVEL_ERROR, "Opening file %s failed",
              filename->str);
      }

      fclose(fp);
    }
    f = g_dir_read_name(dir);
  }
}

static gboolean _isVCardFile(const char *filename) {
  gboolean isVCardFile = FALSE;
  gchar **split = g_strsplit(filename, ".", 0);
  int i = 0;
  while (split && split[i]) {
    if (split[i + 1] == NULL) {
      if (g_strcmp0("vcf", split[i]) == 0) {
        isVCardFile = TRUE;
      }
    }
    i++;
  }
  return isVCardFile;
}

static GString *_getStringValue(vc_component *vc, const char *name) {
  GString *r = NULL;
  vc_component *v = NULL;

  v = vc_get_next_by_name(vc, name);
  if (v) {
    char *val = vc_get_value(v);
    if (val) {
      r = g_string_new(val);
    } else {
      r = g_string_new("");
    }
  }

  return r;
}
