# eagle

Das XMPP eagle Projekt.

* Adressbuch Anzeige für abook-Dateien
* Adressbuch Anzeige für VCards
* Öffentliche OpenPGP Keys via GnuPG
* XMPP Accounts via XMPP Roster
* Senden von signierten-verschlüsselten XMPP-OX Nachrichten

## Dokumentation

* Wiki: https://codeberg.org/eagle/eagle/wiki 
* Homepage: https://pages.codeberg.org/eagle 

## Kontakt

* Chatraum: xmpp:eagle@conference.anoxinon.me?join

## Build

```
git clone --recurse-submodules git@codeberg.org:eagle/eagle.git
cd eagle/libcxmppx/
./bootstrap.sh && ./configure && make
cd ..
./bootstrap.sh && ./configure && make
```
